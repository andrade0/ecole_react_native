// @flow
/* eslint-disable no-console, global-require, no-nested-ternary, react/jsx-indent */
import * as React from "react";
import {SwitchNavigator, DrawerNavigator} from "react-navigation";
import Ecole from "./src/Ecole";
import { Root } from "native-base";

const AppNavigator = SwitchNavigator(
    {
        Ecole: { screen: Ecole }
    }
);
export default () =>
    <Root>
        <AppNavigator />
    </Root>;