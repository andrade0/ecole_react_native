// @flow
/* eslint-disable global-require */
import {Asset} from "expo";

export default class Images {

    static logo = require("./logo.png");
    static login = require("./login.jpg");
    static signUp = require("./signUp.jpg");
    static drawer = require("./drawer.jpg");
    static home = require("./home.jpg");
    static lists = require("./lists.jpg");
    static timeline = require("./timeline.jpg");
    static admin = require("./admin.png");
    static back = require("./back.png");
    static editer = require("./editer.png");
    static eleve = require("./eleve.png");
    static fastforward = require("./fastforward.png");
    static horloge = require("./horloge.png");
    static mail = require("./mail.png");
    static mail2 = require("./mail2.png");
    static marker = require("./marker.png");
    static moniteur = require("./moniteur.png");
    static mute = require("./mute.png");
    static next = require("./next.png");
    static password = require("./password.png");
    static pause = require("./pause.png");
    static playbutton = require("./playbutton.png");
    static replay = require("./replay.png");
    static rewind = require("./rewind.png");
    static shuffle = require("./shuffle.png");
    static speaker = require("./speaker.png");
    static stop = require("./stop.png");
    static tel = require("./tel.png");

    static defaultAvatar = require("./avatars/default-avatar.jpg");
    static avatar1 = require("./avatars/avatar-1.jpg");
    static avatar2 = require("./avatars/avatar-2.jpg");
    static avatar3 = require("./avatars/avatar-3.jpg");

    static foodGroup = require("./groups/food.jpg");
    static workGroup = require("./groups/work.jpg");
    static vacationGroup = require("./groups/vacation.jpg");
    static citiesGroup = require("./groups/cities.jpg");

    static downloadAsync(): Promise<*>[] {
        return [
            Asset.fromModule(Images.logo).downloadAsync(),
            Asset.fromModule(Images.login).downloadAsync(),
            Asset.fromModule(Images.signUp).downloadAsync(),
            Asset.fromModule(Images.drawer).downloadAsync(),
            Asset.fromModule(Images.home).downloadAsync(),
            Asset.fromModule(Images.lists).downloadAsync(),
            Asset.fromModule(Images.timeline).downloadAsync(),
            Asset.fromModule(Images.admin).downloadAsync(),
            Asset.fromModule(Images.back).downloadAsync(),
            Asset.fromModule(Images.editer).downloadAsync(),
            Asset.fromModule(Images.eleve).downloadAsync(),
            Asset.fromModule(Images.fastforward).downloadAsync(),
            Asset.fromModule(Images.horloge).downloadAsync(),
            Asset.fromModule(Images.mail).downloadAsync(),
            Asset.fromModule(Images.mail2).downloadAsync(),
            Asset.fromModule(Images.marker).downloadAsync(),
            Asset.fromModule(Images.moniteur).downloadAsync(),
            Asset.fromModule(Images.mute).downloadAsync(),
            Asset.fromModule(Images.next).downloadAsync(),
            Asset.fromModule(Images.password).downloadAsync(),
            Asset.fromModule(Images.pause).downloadAsync(),
            Asset.fromModule(Images.playbutton).downloadAsync(),
            Asset.fromModule(Images.replay).downloadAsync(),
            Asset.fromModule(Images.rewind).downloadAsync(),
            Asset.fromModule(Images.shuffle).downloadAsync(),
            Asset.fromModule(Images.speaker).downloadAsync(),
            Asset.fromModule(Images.stop).downloadAsync(),
            Asset.fromModule(Images.tel).downloadAsync(),

            Asset.fromModule(Images.defaultAvatar).downloadAsync(),
            Asset.fromModule(Images.avatar1).downloadAsync(),
            Asset.fromModule(Images.avatar2).downloadAsync(),
            Asset.fromModule(Images.avatar3).downloadAsync(),

            Asset.fromModule(Images.foodGroup).downloadAsync(),
            Asset.fromModule(Images.workGroup).downloadAsync(),
            Asset.fromModule(Images.vacationGroup).downloadAsync(),
            Asset.fromModule(Images.citiesGroup).downloadAsync()
        ];
    }
}
