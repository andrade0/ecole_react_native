import React, {Component, PropTypes} from "react";
import {observer} from 'mobx-react';
import UserModel from './models/UserModel';
import AppModel from './models/AppModel';
import Login from './Login';
import Intro from './Intro';
import Router from './Router';
import {AppLoading, Font} from 'expo';
import FontAwesome from './fonts/FontAwesome.ttf';
import MaterialIcons from './fonts/MaterialIcons.ttf';
import Roboto_medium from './fonts/Roboto_medium.ttf';
import OpenSans_Regular from './fonts/OpenSans-Regular.ttf';
import Ionicons from './fonts/Ionicons.ttf';

import Lesson from './views/Lesson';
import LessonForm from "./views/LessonForm";
import Rate from "./views/Rate";

@observer
class Ecole extends Component {
    constructor() {
        super();

        this.state = {
            fontLoaded: false
        };
    }

    async componentWillMount() {
        try {
            await Font.loadAsync({
                FontAwesome,
                MaterialIcons,
                Roboto_medium,
                Ionicons,
                OpenSans_Regular
            });
            this.setState({fontLoaded: true});
        } catch (error) {
            console.log('error loading icon fonts', error);
        }
    }

    render() {
        if (!this.state.fontLoaded || UserModel.loading ) {
            return <AppLoading />;
        }

        //return <Rate lid={32} />;

        if (UserModel.uid) {
            return <Router />
        } else {

            if (AppModel.walkthroutOk) {
                return (
                    <Login />
                );
            } else {
                return (
                    <Intro />
                );
            }


        }
    }
}

Ecole.propTypes = {};

export default Ecole;


