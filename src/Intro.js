import React, {Component, PropTypes} from "react";
import {observer} from "mobx-react";
import {Dimensions, Image, ScrollView, StyleSheet} from "react-native";
import {Button, Container, Content, Text, View} from "native-base";
import AppModel from './models/AppModel';
import {Constants} from 'expo';
import Carousel from 'react-native-looped-carousel';

const {width, height} = Dimensions.get('window');

@observer
class Intro extends React.Component<Props, State> {
    constructor() {
        super();
        this.state = {
            size: {width, height},
        };
    }
    _onLayoutDidChange = e => {
        const layout = e.nativeEvent.layout;
        this.setState({size: {width: layout.width, height: layout.height}});
    };
    render() {
        const slides = [
            {
                key: '0',
                title: 'L’application de suivi élève',
                text: 'L’expérience utilisateur est une priorité pour nous. Nous mettons à votre disposition gratuitement les meilleurs outils.',
                image: require('./images/business-agreement-icon.png'),
            },
            {
                key: '1',
                title: 'Gérez ici tout ce dont vous avez besoin',
                text: 'Notifications, planning, commentaires des moniteurs, contacter de l’auto-école…',
                image: require('./images/internet-explorer-icon.png'),
            }
        ];
        return (
            <View style={{flex: 1, backgroundColor: '#1D1C1C', width: '100%', height: '100%'}} onLayout={this._onLayoutDidChange}>
                <Carousel
                    delay={2000}
                    style={this.state.size}
                    autoplay
                    pageInfo={false}
                    currentPage={1}
                    bullets={true}
                    onAnimateNextPage={(p) => {}}>
                    {slides.map(image => (
                        <View key={image.key} style={[styles.mainContent, this.state.size]}>
                            <Image style={{width: 300, height: 140, marginBottom: 10, marginTop: 10}} source={require("./images/logo2.png")} alt="logo"/>
                            <Image style={{height: 140, width: 140, marginBottom: 10}} source={image.image}/>
                            <Text style={styles.title}>{image.title}</Text>
                            <Text style={styles.text}>{image.text}</Text>
                            <Button onPress={() => {
                                AppModel.walkthroutOk = true
                            }} style={{backgroundColor: "#BB2625", marginTop: 2, marginRight: 20, marginLeft: 20}}
                                    block><Text style={{fontFamily: 'OpenSans_Regular'}}>S'IDENTIFIER</Text></Button>
                        </View>
                    ))}
                </Carousel>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scrollContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
    },
    mainContent: {
        flex: 1,
        width,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        marginVertical: 24,
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'white',
        height: 80,
        width: 250,
        marginBottom: 10, fontFamily: 'OpenSans_Regular'
    },
    text: {
        marginBottom: 8,
        fontSize: 18,
        textAlign: 'center',
        color: 'white',
        height: 100, fontFamily: 'OpenSans_Regular'
    },
});

export default Intro;

