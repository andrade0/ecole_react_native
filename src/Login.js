import React, {PropTypes} from "react";
import {observer} from "mobx-react";
import UserModel from "./models/UserModel";
import AppModel from "./models/AppModel";
import {AsyncStorage, Dimensions, Image} from "react-native";
import {Button, Container, Content, Form, Icon, Input, Item, Text, View} from "native-base";

@observer
class Login extends React.Component<Props, State> {
    constructor() {
        super();
        this.state = {
            password: "",
            mode: "login",
            loading: false
        };
    }

    componentDidMount() {
        if (!UserModel.loading && !UserModel.uid) {
            AsyncStorage.getItem("ecole_mail").then((ecoleMail) => {
                if (ecoleMail) {
                    AppModel.mail = ecoleMail;
                }
            });
        }
    }

    render() {
        let {height, width} = Dimensions.get('window');
        return (<Container style={{width: '100%', height: '100%', backgroundColor: '#1D1C1C', padding: 20}} >
            <Content>
                <View style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    marginBottom: 80,
                    marginTop: 60
                }}>
                    <Image style={{width: 240, height: 100}} source={require("./images/logo2.png")} alt="logo"/>
                </View>
                <Form style={{marginTop: 0}}>
                    <Item rounded style={{backgroundColor: 'white', marginBottom: 20}}>
                        <Icon type="FontAwesome" style={{color: "#C2342A"}} name="at"/>
                        <Input style={{fontFamily: 'OpenSans_Regular'}} placeholder='Email' value={AppModel.mail ? AppModel.mail : ""} onChangeText={(Text2) => {
                            AppModel.mail = Text2;
                            if (Text2 && Text2.length > 0) {
                                AsyncStorage.setItem("ecole_mail", Text2);
                            }
                        }}/>
                    </Item>
                    {this.state.mode === "login" ? <Item rounded style={{backgroundColor: 'white'}}>
                        <Icon type="FontAwesome" style={{color: "#C2342A"}} name="key"/>
                        <Input secureTextEntry placeholder='Mot de passe'
                               value={this.state.password ? this.state.password : ""}
                               onChangeText={(Text) => {
                                   this.setState({password: Text});
                               }}/>
                    </Item> : <View/>}
                    <View style={{marginTop: 16, marginBottom: height - 700, paddingLeft: 16}}>
                        {this.state.mode === "login" ? <Text
                            style={{marginBottom: 10, color: 'white', fontFamily: 'OpenSans_Regular'}}
                            onPress={() => {
                                this.setState({mode: "password"});
                            }}
                        >Mot de passe oublié ?
                        </Text> : <Text
                            style={{marginBottom: 10, color: 'white', fontFamily: 'OpenSans_Regular'}}
                            onPress={() => {
                                this.setState({mode: "login"});
                            }}
                        >Retour à la page de connection
                        </Text>}
                    </View>
                    {this.state.mode === "login" ? <Button
                        onPress={() => {
                            UserModel.login(AppModel.mail, this.state.password);
                        }}
                        block
                        style={{
                            backgroundColor: "#BB2625",
                            marginTop: 120
                        }}
                    ><Text style={{color: "white", fontFamily: 'OpenSans_Regular'}}>Se connecter</Text>
                        <Icon type="Ionicons" style={{color: "white"}} name="md-arrow-forward"/>
                    </Button> : <Button onPress={() => {
                        this.setState({loading: true});
                        AppModel.request("sendNewPasswordEmail", {mail: AppModel.mail}, (response) => {
                            this.setState({loading: false, mode: "login"});
                        });
                    }} block style={{
                        backgroundColor: "#BB2625",
                        marginTop: 190
                    }}>
                        <Text style={{color: "white", fontFamily: 'OpenSans_Regular'}}>{this.state.loading ? "Chargement ..." : "Réinitialiser votre mot de passe"}</Text>
                        <Icon type="Ionicons" style={{color: "white"}} name="md-arrow-forward"/>
                    </Button>}
                </Form>
                {/*<Button onPress={()=>{UserModel.login("andradeolivier@gmail.com", "030303cC");}}><Text>manager</Text></Button>
                <Button onPress={()=>{UserModel.login("andradeolivier+monitor1@gmail.com", "030303cC");}}><Text>monitor</Text></Button>
                <Button onPress={()=>{UserModel.login("andradeolivier+eleve1@gmail.com", "030303cC");}}><Text>Eleve</Text></Button>*/}
            </Content>
        </Container>);
    }
}


Login.propTypes = {};

export default Login;

