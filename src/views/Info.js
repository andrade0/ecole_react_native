import React, { Component, PropTypes } from "react";
import { observer } from "mobx-react";
import RouterModel from "../models/RouterModel";
import AppModel from "../models/AppModel";
import MenuIconList from './conponents/MenuIconList';
import { AppRegistry, Image, StyleSheet, TextInput, Linking, ImageBackground } from "react-native";
import { Container, Header, Content, Form, Item, Label, Button, Input, Left, Body, Title, Right, Text, View, List, ListItem, Icon, Thumbnail } from "native-base";
import ScreenHeader from "./conponents/ScreenHeader";


@observer
class Info extends Component {
    constructor() {
        super();

        this.state = {menu: [
            {
                icon: "call",
                action: () => { Linking.openURL("tel:+33231810847"); },
                title: "Nous appeler",
                type: "Ionicons"
            },
            {
                icon: "mail",
                action: () => { Linking.openURL("mailto:autoecoledeauville@gmail.com"); },
                title: "Nous écrire",
                type: "Ionicons"
            },
            {
                icon: "sitemap",
                action: () => { Linking.openURL("http://auto-ecole-deauville.com"); },
                title: "Notre site web",
                type: "FontAwesome"
            },
            {
                icon: "pin",
                action: () => { Linking.openURL("https://www.google.com/search?client=firefox-b-ab&ei=IH-4W-DGBsOua5XOiXA&q=l%27auto%20ecole%20en%20deauville&oq=l%27auto+ecole+en+deauville&gs_l=psy-ab.3..0i22i30k1.15630.16764.0.16852.9.7.0.0.0.0.172.172.0j1.1.0....0...1c.1.64.psy-ab..8.1.172....0.uqbxAp8KcOs&npsic=0&rflfq=1&rlha=0&rllag=49358548,76489,226&tbm=lcl&rldimm=437201050284843767&ved=2ahUKEwj82oGXv_HdAhUSyxoKHTUIDGQQvS4wAHoECAAQIQ&rldoc=1&tbs=lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:2#rlfi=hd:;si:437201050284843767;mv:!3m12!1m3!1d4317.323463475207!2d0.08144615!3d49.3613368!2m3!1f0!2f0!3f0!3m2!1i481!2i319!4f13.1"); },
                title: "Nous rejoindre",
                type: "Ionicons"
            },
            {
                icon: "logo-instagram",
                action: () => { Linking.openURL("https://www.instagram.com/explore/locations/934600376632378/auto-ecole-deauville/"); },
                title: "Instagram",
                type: "Ionicons"
            },
            {
                icon: "logo-facebook",
                action: () => { Linking.openURL("https://www.facebook.com/pg/autoecoledeauville/reviews/?ref=page_internal"); },
                title: "Donner un avis sur Facebook",
                type: "Ionicons"
            },
            {
                icon: "logo-google",
                action: () => { Linking.openURL("https://www.google.com/search?q=auto+ecole+deauville+avis+google&ie=utf-8&oe=utf-8&client=firefox-b-ab#lrd=0x47e1d4b1d66c7ecb:0x6114009f1a7e2f7,3,,,"); },
                title: "Donner un avis sur Google",
                type: "Ionicons"
            }
        ]};
    }

    render() {

        return (
            <Container style={{width: "100%", height: "100%", backgroundColor: '#1D1C1C'}}>
                <ScreenHeader textColor="#ffffff" bigTitle="Informations pratiques" backButtonAction={()=>{RouterModel.back()}} />
                <Text style={{color: "#BB2625", fontSize: 30, marginLeft: 18, marginBottom: 18, fontFamily: 'OpenSans_Regular'}}>Informations pratiques</Text>
                <Content style={{marginTop: 0}} padder>
                    <MenuIconList menu={this.state.menu} />
                </Content>
            </Container>
        );
    }
}

Info.propTypes = {

};

export default Info;

