import React, { Component, PropTypes } from "react";
import { observer } from 'mobx-react';
import RouterModel from '../models/RouterModel';
import AppModel from '../models/AppModel';
import { AppRegistry, Image, StyleSheet, TextInput } from "react-native";
import { Container, Header, Content, Form, Item, Label, Button, Input, Left, Body, Title, Right, Text, View, List, ListItem, Icon, Thumbnail } from "native-base";
import ScreenHeader from "./conponents/ScreenHeader";



@observer
class Videos extends Component {
  constructor() {
    super();
    this.state = {
      items: []
    }
  }

  componentWillMount(){
    AppModel.request('getVideos', {}, (response)=>{
      if(response && response.status && response.status === "success"){
        this.setState({items: response.data});
      }
    });
  }

  render() {

      return (
          <Container style={{width: "100%", height: "100%", backgroundColor: '#1D1C1C'}}>
              <ScreenHeader textColor="#611214" screenTitle="Videos" backButtonAction={()=>{RouterModel.goto('home', null)}} />
              <Text style={{color: "#BB2625", fontSize: 30, marginLeft: 18, marginBottom: 18, fontFamily: 'OpenSans_Regular'}}>Videos</Text>
              <Content padder style={{ backgroundColor: "#fff" }}>
                  <List>
                      {this.state.items.map((item)=>{
                         return <ListItem button onPress={()=>{RouterModel.goto('video', {video: item})}} key={`${item.vid}videos`} thumbnail>
                             <Left>
                                 <Thumbnail square source={{ uri: item.image }} />
                             </Left>
                             <Body>
                             <Text>{item.title}</Text>
                             <Text note numberOfLines={1}>{item.description}</Text>
                             </Body>
                             <Right>
                                 <Button transparent>
                                     <Text style={{fontFamily: 'OpenSans_Regular'}} onPress={()=>{
                                         RouterModel.goto('video', {video: item});
                                     }}><Icon style={{color: "#000", fontSize: 20}} type="Ionicons" name="eye" /></Text>
                                 </Button>
                             </Right>
                         </ListItem>
                      })}
                  </List>
              </Content>
          </Container>
      );
  }
}

Videos.propTypes = {

};

export default Videos;


