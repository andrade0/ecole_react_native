import React, { Component, PropTypes } from "react";
import { observer } from 'mobx-react';
import RouterModel from '../models/RouterModel';
import AppModel from '../models/AppModel';
import { AppRegistry, Image, StyleSheet, TextInput, ImageBackground } from "react-native";
import { Container, Header, Content, Form, Item, Label, Button, Input, Left, Body, Title, Right, Text, View, List, ListItem, Icon, Thumbnail } from "native-base";
import ScreenHeader from "./conponents/ScreenHeader";



@observer
class Lexique extends Component {
    constructor() {
        super();
        this.state = {
            items: [
                {id: 7,  title:"TAD"	    ,description: "Tourner à droite"},
                {id: 8,  title:"TAG"	    ,description: "Tourner à gauche"},
                {id: 9,  title:"AM"	        ,description: "Angle mort"},
                {id: 10, title:"CRD"	    ,description: "Créneau droit"},
                {id: 20, title:"CRG"	    ,description: "Créneau gauche"},
                {id: 11, title:"BV"	        ,description: "Boîte de vitesse"},
                {id: 12, title:"RGB avant"	,description: "Rangement Bataille avant"},
                {id: 13, title:"PLAC"	    ,description: "Placements"},
                {id: 14, title:"OBS"	    ,description: "Observation"},
                {id: 15, title:"ANT"	    ,description: "Anticipation"},
                {id: 16, title:"PB MECA"    ,description: "Probleme mécanique"},
                {id: 17, title:"RB"	        ,description: "Rangement bataille"},
                {id: 18, title:"DT"	        ,description: "Demi tour"}
            ]
        };
    }

    render() {
        return (
            <Container style={{width: "100%", height: "100%", backgroundColor: '#1D1C1C'}}>
                <ScreenHeader screenTitle="Lexique" backButtonAction={()=>{RouterModel.goto("home", {})}} />
                <Text style={{color: "#BB2625", fontSize: 30, marginLeft: 18, marginBottom: 18, fontFamily: 'OpenSans_Regular'}}>Lexique</Text>
                <View padder>

                    <Text style={{color: "#ffffff", fontStyle: "italic", width: 250, marginLeft: 20, fontFamily: 'OpenSans_Regular'}}><Icon style={{color: "#ffffff", fontSize: 15}} type="FontAwesome" name="arrow-right" /> ci retrouvez tous les termes techniques que vos moniteurs utilisent dans les commentaires</Text>
                </View>
                <Content padder>
                    <List>
                        {this.state.items.map((item)=>{
                            return <ListItem key={item.id}>
                                <Text style={{fontWeight: 'bold',fontFamily: "OpenSans_Regular", color: "#ffffff"}}>{item.title} : </Text>
                                <Text style={{fontFamily: "OpenSans_Regular", color: "#ffffff"}}>{item.description}</Text>
                            </ListItem>
                        })}
                    </List>
                </Content>
            </Container>
        );
    }
}

Lexique.propTypes = {

};

export default Lexique;


