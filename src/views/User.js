import React, {Component, PropTypes} from "react";
import {observer} from "mobx-react";
import RouterModel from "../models/RouterModel";
import AppModel from "../models/AppModel";
import UserModel from "../models/UserModel";
import async from "async";
import LessonListed from "./conponents/LessonListed";
import { AppRegistry, Image, StyleSheet, ImageBackground, Alert, Linking, Dimensions } from "react-native";
import { Container, Header, Content, Button, Left, Body, Title, Right, Text, View, Icon, Spinner, List, Fab, FooterTab, Badge, Footer, Accordion } from "native-base";
import ScreenHeader from "./conponents/ScreenHeader";

const boy = require('../images/boy.png');
const girl = require('../images/woman.png');

@observer
class User extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null,
            lessons: [],
            past_lessons: [],
            active: false,
            currentPage: "info",
            title: "Informations"
        };
        this.releoadlessons = this.releoadlessons.bind(this);
    }

    releoadlessons(cb2) {
        async.waterfall([
            (cb) => {
                let uid = null;
                if (this.props.uid) {
                    uid = this.props.uid;
                } else if (this.state.user && this.state.user.uid) {
                    uid = this.state.user.uid;
                }
                if (uid) {
                    AppModel.request("getUserLessons", {uid}, (response) => {
                        if (response && response.data) {
                            this.setState({lessons: response.data});
                        }
                        cb();
                    });
                } else {
                    cb();
                }
            },
            (cb) => {
                let uid = null;
                if (this.props.uid) {
                    uid = this.props.uid;
                } else if (this.state.user && this.state.user.uid) {
                    uid = this.state.user.uid;
                }
                if (uid) {
                    AppModel.request("getUserPastLessons", {uid: this.state.user.uid}, (response) => {
                        if (response && response.data) {
                            this.setState({past_lessons: response.data});
                        }
                        cb();
                    });
                } else {
                    cb();
                }
            }
        ], () => {
            cb2();
        });
    }

    componentWillMount() {
        async.waterfall([
            (cb) => {
                if (this.props.user) {
                    this.setState({user: this.props.user});
                    cb();
                } else if (this.props.uid) {
                    AppModel.request("getUser", {uid: this.props.uid}, (response) => {
                        if (response && response.status && response.status === "success" && response.data) {
                            this.setState({user: response.data});
                            cb();
                        } else {
                            cb();
                        }
                    });
                }
            },
            (cb) => {
                let uid = null;
                if (this.props.uid) {
                    uid = this.props.uid;
                } else if (this.props.user && this.props.user.uid) {
                    uid = this.props.user.uid;
                }
                if (uid) {
                    AppModel.request("getUserLessons", {uid}, (response) => {
                        if (response && response.data) {
                            this.setState({lessons: response.data});
                        }
                        cb();
                    });
                } else {
                    cb();
                }
            },
            (cb) => {
                let uid = null;
                if (this.props.uid) {
                    uid = this.props.uid;
                } else if (this.props.user && this.props.user.uid) {
                    uid = this.props.user.uid;
                }
                if (uid) {
                    AppModel.request("getUserPastLessons", {uid}, (response) => {
                        if (response && response.data) {
                            this.setState({past_lessons: response.data});
                        }
                        cb();
                    });
                } else {
                    cb();
                }
            }
        ], () => {

        });
    }

    componentWillUnmount() {

    }

    render() {
        let {height, width} = Dimensions.get('window');
        if (this.state.user) {

            let role_str = "Elève";
            if(this.state.user.role === 2){
                role_str = "Moniteur";
            } else if(this.state.user.role === 3){
                role_str = "Manager";
            }

            let content = null;
            let bg = require("../images/backautoecole.png");
            switch (this.state.currentPage) {
                case "future":
                    bg = require("../images/bglist.png");
                    content = this.state.lessons && this.state.lessons.length > 0?<List>{this.state.lessons.map((lesson) => <LessonListed releoadlessons={this.releoadlessons} user={this.state.user} lesson={lesson} key={lesson.lid} />)}</List>:<Text style={{color: "#ffffff", padding: 10}}>Pas de leçon pour le moment</Text>;
                    break;
                case "past":
                    bg = require("../images/bglist.png");
                    content = this.state.past_lessons && this.state.past_lessons.length > 0?<List>{this.state.past_lessons.map((lesson) => <LessonListed releoadlessons={this.releoadlessons} user={this.state.user} lesson={lesson} key={lesson.lid} />)}</List>:<Text style={{color: "#ffffff", padding: 10}}>Pas de leçon pour le moment</Text>;
                    break;
                default:
                    let doneHours = <Text></Text>;
                    if(this.state.user.done_lessons && parseInt(this.state.user.done_lessons) && parseInt(this.state.user.done_lessons) > 0) {
                        doneHours = <View style={{ display: "flex", flexDirection: "row", justifyContent: "flex-start", marginBottom: 10 }}>
                            <Image source={require('../images/heureeffectueesemoji.png')} style={{width: 28, height: 28, marginRight: 10}} />
                            <Text style={{color: "white", fontSize: 15, fontFamily: "OpenSans_Regular"}}>{this.state.user.done_lessons} heure(s) de conduite effectuée(s)</Text>
                        </View>;
                    }
                    content = <View style={{padding: 15}}>
                            <View style={{display: "flex", alignItems: "flex-start", justifyContent: "center", marginBottom: 30}}>
                                <Text style={{color: '#BB2625', fontSize: 35, fontFamily: "OpenSans_Regular"}}>{`Mon compte`}</Text>
                            </View>
                        <View style={{ display: "flex", flexDirection: "row", justifyContent: "flex-start", marginBottom: 10 }}>
                            <Image source={this.state.user.picture && this.state.user.picture.length > 10?{uri: this.state.user.picture}:require('../images/filleemoji.png')} style={{width: 28, height: 28, marginRight: 10}} />
                            <Text style={{color: "white", fontSize: 15, fontFamily: "OpenSans_Regular"}}>{`${this.state.user.firstname} ${this.state.user.lastname}`}</Text>
                        </View>
                        <View style={{ display: "flex", flexDirection: "row", justifyContent: "flex-start", marginBottom: 10 }}>
                            <Image source={require('../images/emailemoji.png')} style={{width: 28, height: 28, marginRight: 10}} />
                            <Text style={{color: "white", fontSize: 15, fontFamily: "OpenSans_Regular"}}>{this.state.user.mail}</Text>
                        </View>
                        <View style={{ display: "flex", flexDirection: "row", justifyContent: "flex-start", marginBottom: 10 }}>
                            <Image source={require('../images/mapmarkeremoji.png')} style={{width: 28, height: 28, marginRight: 10}} />
                            <Text style={{color: "white", fontSize: 15, fontFamily: "OpenSans_Regular"}}>{this.state.user.adresse}</Text>
                        </View>
                        <View style={{ display: "flex", flexDirection: "row", justifyContent: "flex-start", marginBottom: 10 }}>
                            <Image source={require('../images/phoneemoji.png')} style={{width: 28, height: 28, marginRight: 10}} />
                            <Text style={{color: "white", fontSize: 15, fontFamily: "OpenSans_Regular"}}>{this.state.user.tel}</Text>
                        </View>
                        <View style={{ display: "flex", flexDirection: "row", justifyContent: "flex-start", marginBottom: 10 }}>
                            <Image source={require('../images/evaldepartemoji.png')} style={{width: 28, height: 28, marginRight: 10}} />
                            <Text style={{color: "white", fontSize: 15, fontFamily: "OpenSans_Regular"}}>Evaluation de départ : {this.state.user.evaluationDepart} heures</Text>
                        </View>
                        {doneHours}
                        {UserModel.role !== 1?<Button rounded style={{marginTop: 10,borderColor: "#ffffff", width: 280, height: 70, marginLeft: "auto", marginRight: "auto"}} onPress={()=>{Linking.openURL("tel:"+this.state.user.tel)}} block bordered>
                            <Icon name="call" style={{color: "#ffffff"}} /><Text style={{color: "#ffffff", fontSize: 20}}>Appeler {this.state.user.firstname}</Text>
                        </Button>:null}
                    </View>;
                    break;
            }



            return (
                <Container style={{width: "100%", height: "100%", backgroundColor: '#1D1C1C'}}>
                    <ScreenHeader textColor="#611214" nologo={true} backButtonAction={()=>{
                        if(UserModel.role === 1){
                            RouterModel.goto("home", {});
                        } else {
                            RouterModel.goto("users", {});
                        }
                    }} />
                    <Content>{content}</Content>
                    {UserModel.role !== 2 && UserModel.role !== 1?<Fab direction="down"
                                               position="topRight"
                                               active={this.state.active}
                                               containerStyle={{ }}
                                               style={{ backgroundColor: '#C2342A' }}
                                               onPress={() => this.setState({ active: !this.state.active })}>
                        <Icon type="Ionicons" name="cog" />
                        {UserModel.role === 3?<Button style={{ backgroundColor: '#C2342A' }} onPress={() => {
                            Alert.alert(
                                "",
                                "êtes vous sûr de vouloir supprimer cette utilisateur ?",
                                [
                                    {text: "Oui",
                                        onPress: () => {
                                            AppModel.request("deleteUser", {uid: this.state.user.uid}, () => {
                                                RouterModel.goto('users', {});
                                            });
                                        }},
                                    {text: "Non",
                                        onPress: () => {

                                        },
                                        style: "cancel"}
                                ],
                                { cancelable: false }
                            );
                        }}>
                            <Icon type="Ionicons" name="trash" />
                        </Button>:null}
                        {UserModel.role === 3 || this.state.user.uid === UserModel.uid?<Button style={{ backgroundColor: "#C2342A" }} onPress={()=>{
                            RouterModel.goto("userForm", {uid: this.state.user.uid});
                        }}>
                            <Icon type="FontAwesome" name="edit" />
                        </Button>:null}
                        {UserModel.role === 3 && this.state.user.role === 1?<Button style={{ backgroundColor: "#C2342A" }} onPress={() => { RouterModel.goto("lessonForm", {user: this.state.user}); }}>
                            <Icon
                                type="Ionicons"
                                style={{fontSize: 40, color: "#ffffff"}}
                                name="add" />
                        </Button>:null}
                    </Fab>:<Text></Text>}
                    <Footer>
                        <FooterTab style={{backgroundColor:"#BB2625"}}>
                            <Button style={{backgroundColor:this.state.currentPage === "info"?"#FFFFFF":"#BB2625"}} active={this.state.currentPage === "info"} vertical onPress={()=>{
                                this.setState({currentPage: "info", title: "informations"});
                            }}>
                                <Icon style={{color:this.state.currentPage === "info"?"#BB2625":"#FFFFFF"}} type="FontAwesome" name="id-card" />
                                <Text style={{color:this.state.currentPage === "info"?"#BB2625":"#FFFFFF", fontFamily: 'OpenSans_Regular'}}>Infos</Text>
                            </Button>
                            <Button  style={{backgroundColor:this.state.currentPage === "future"?"#FFFFFF":"#BB2625"}} active={this.state.currentPage === "future"} badge vertical onPress={()=>{
                                this.setState({currentPage: "future", title: "Leçons à venir"});
                            }}>
                                <Badge><Text>{this.state.lessons.length}</Text></Badge>
                                <Icon style={{color:this.state.currentPage === "future"?"#BB2625":"#FFFFFF"}} type="FontAwesome" name="car" />
                                <Text style={{color:this.state.currentPage === "future"?"#BB2625":"#FFFFFF", fontFamily: 'OpenSans_Regular'}}>A venir</Text>
                            </Button>
                            <Button style={{backgroundColor:this.state.currentPage === "past"?"#FFFFFF":"#BB2625"}} active={this.state.currentPage === "past"} badge vertical onPress={()=>{
                                this.setState({currentPage: "past", title: "Leçons passées"});
                            }}>
                                <Badge><Text>{this.state.past_lessons.length}</Text></Badge>
                                <Icon style={{color:this.state.currentPage === "past"?"#BB2625":"#FFFFFF"}} type="Ionicons" name="clock" />
                                <Text style={{color:this.state.currentPage === "past"?"#BB2625":"#FFFFFF", fontFamily: 'OpenSans_Regular'}}>Passées</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </Container>
            );
        }
        return (<Spinner />);

    }
}

User.propTypes = {};

export default User;
