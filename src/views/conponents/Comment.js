import React, { Component, PropTypes } from "react";
import { observer } from 'mobx-react';
import RouterModel from '../../models/RouterModel';
import AppModel from '../../models/AppModel';
import UserModel from '../../models/UserModel';
import { AppRegistry, View, Image, StyleSheet, Text, Button, TextInput, Textarea, Form } from 'react-native';

@observer
class Comment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: props.lesson.comment?props.lesson.comment:'',
      message: '',
      loading: false
    };
  }

  componentDidMount(){

  }

  componentWillMount(){

  }

  componentWillUnmount() {

  }

  render() {

    let comment = <View><Text style={{color: '#DDDDDD', fontFamily: 'OpenSans_Regular'}}>Commentaire du moniteur:</Text><br />{this.state.comment}</View>;
    let comment_button = null;
    if(UserModel.role === 2 || UserModel.role === 3){
      comment = <View style={{marginTop: 10}}>Commentaire: <br /><TextInputarea rows={3} style={{width: '100%'}} value={this.state.comment} onChange={(e) => this.setState({comment: e.target.value})} /></View>;
      comment_button = <Button style={{marginTop: 5}} onPress={()=>{
        this.setState({loading: true});
        AppModel.request('comment', {lid: this.props.lesson.lid, comment: this.state.comment}, (response)=>{
            console.log(response);
          this.setState({loading: false});
        if(response.status === "success"){
          RouterModel.goto('user', {user: this.props.lesson.eleve});
        } else if(response.message){
          this.setState({message: response.message});
        }
      })}} label="Enregistrer le commentaire" />;
    }

    if(this.state.loading){
      return (<ProgressSpinner />);
    } else {
      return (
        <View>
          {this.state.messages?<View>{this.state.messages}</View>:''}
          <View style={{fontFamily: 'OpenSans_Regular'}}>{comment}</View>
          <View style={{fontFamily: 'OpenSans_Regular'}}>{comment_button}</View>
        </View>
      );
    }
  }
}

Comment.propTypes = {

};

export default Comment;


