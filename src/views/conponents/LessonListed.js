import React, { Component, PropTypes } from "react";
import { observer } from 'mobx-react';
import AppModel from '../../models/AppModel';
import UserModel from '../../models/UserModel';
import RouterModel from '../../models/RouterModel';
import AdminButton from './AdminButton';
import Comment from './Comment';
import moment from "moment";
import { AppRegistry, Image, StyleSheet, TextInput } from 'react-native';
import { Container, Header, Content, Form, Item, Label, Button, Input, Left, Body, Title, Right, Text, View, Card, CardItem, Icon, Spinner, List, ListItem, Accordion } from 'native-base';


@observer
class LessonListed extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        const timing = AppModel.moment(this.props.lesson.timing);
        const month = timing.format("MMMM");
        const dayOfMonth = timing.format("D");
        const dayOfWeek = timing.format("dddd");
        const heure_de_la_lecon = timing.format("HH:mm");
        return (
        <ListItem button onPress={()=>{
            RouterModel.goto("lesson", {lid: this.props.lesson.lid})
        }}>
            <Left>
                <Icon type="FontAwesome" name="circle" style={{marginTop: 7, fontSize: 10, marginRight: 0, color: "#ffffff"}} /><Text style={{fontFamily: "OpenSans_Regular", color: "#ffffff"}}>{dayOfWeek} {dayOfMonth} {month} à {heure_de_la_lecon}</Text>
            </Left>
            <Right>
                <Icon style={{color: "#ffffff"}} name="arrow-forward" />
            </Right>
        </ListItem>
        );
    }
}

LessonListed.propTypes = {

};

export default LessonListed;


