import React, { Component, PropTypes } from "react";
import { observer } from 'mobx-react';
import RouterModel from '../../models/RouterModel';
import AppModel from '../../models/AppModel';
import AdminButton from './AdminButton';
import { AppRegistry, Image, StyleSheet, TextInput } from 'react-native';
import { Container, Header, Content, Form, Item, Label, Button, Input, Left, Body, Title, Right, Text, View, Card, CardItem, Icon, List, ListItem, Thumbnail } from 'native-base';

const boy = require('../../images/boy.png');
const girl = require('../../images/woman.png');

@observer
class UserListed extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount(){

  }

  componentWillMount(){

  }

  componentWillUnmount() {

  }

  render() {
      let sex = true;
      if(this.props.user.gender === "f"){
          sex = false;
      }

      return (<ListItem style={{width: '90%'}} avatar onPress={()=>{RouterModel.goto('user', {user: this.props.user})}}>
              <Left>
                  {sex?<Thumbnail source={this.props.user && this.props.user.picture && this.props.user.picture.length > 10?{uri: this.props.user.picture}:require('../../images/homme.png')} style={{width: 35, height: 35, marginRight: 10}}/>:<Thumbnail source={this.props.user.picture && this.props.user.picture.length > 10?{uri: this.props.user.picture}:require('../../images/femme.png')} style={{width: 35, height: 35, marginRight: 10}}/>}
              </Left>
              <Body>
                <Text style={{fontSize: 15, color: "#ffffff", fontFamily: "OpenSans_Regular"}}>{this.props.user.firstname} {this.props.user.lastname}</Text>
              </Body>
              <Right>
                <Icon style={{fontSize: 16, color: "#C2342A"}} name="arrow-forward" />
              </Right>
            </ListItem>);

      return (<ListItem button onPress={()=>{RouterModel.goto('user', {user: this.props.user})}}>
          <Left>
              {sex?<Image source={this.props.user && this.props.user.picture && this.props.user.picture.length > 10?{uri: this.props.user.picture}:require('../../images/homme.png')} style={{width: 26, height: 26, marginRight: 10}}/>:<Image source={this.props.user.picture && this.props.user.picture.length > 10?{uri: this.props.user.picture}:require('../../images/femme.png')} style={{width: 30, height: 30, marginRight: 10}}/>}
              <Text style={{fontSize: 16, color: "#ffffff", fontFamily: "OpenSans_Regular"}}>{this.props.user.firstname} {this.props.user.lastname}</Text>
          </Left>
          <Right>
              <Icon style={{color: "#C2342A"}} name="arrow-forward" />
          </Right>
      </ListItem>);
  }
}

UserListed.propTypes = {

};

export default UserListed;


