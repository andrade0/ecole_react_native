import React, { Component, PropTypes } from "react";
import { observer } from "mobx-react";
import { Image, Alert } from "react-native";
import { Text } from "native-base";
import RouterModel from "../../models/RouterModel";
import UserModel from "../../models/UserModel";

const edit = require("../../icons/036-edit.png");
const addlesson = require("../../icons/003-event.png");
const deleteIcon = require("../../images/back.png");

@observer
class AdminButton extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        if (this.props.icon && this.props.roles && this.props.action && this.props.roles) {

            const access = this.props.roles.reduce((c, v) => {
                if (v === UserModel.role) {
                    c = true;
                }
                return c;
            }, false);


            if (access) {
                let icon = null;
                switch (this.props.icon) {
                case "edit":
                    icon = edit;
                    break;
                case "addLEsson":
                    icon = addlesson;
                    break;
                default:
                    break;
                }

                if (icon) {
                    let style = {};
                    if (this.props.style) {
                        style = this.props.style;
                    }
                    let args = {};
                    if (this.props.style) {
                        args = this.props.args;
                    }
                    return <Image onPress={() => { RouterModel.goto(this.props.action, args); }} source={icon} />;
                }
                return <Text>nada</Text>;

            }
            return <Text>nada</Text>;


        } else if (this.props.icon && this.props.prompt && this.props.callback && this.props.roles && (this.props.icon === "delete" || this.props.icon === "remove")) {
            const access = this.props.roles.reduce((c, v) => {
                if (v === UserModel.role) {
                    c = true;
                }
                return c;
            }, false);

            if (access) {
                let style = {};
                if (this.props.style) {
                    style = this.props.style;
                }
                return (<Image
                    source={deleteIcon}
                    style={{cursor: "pointer", ...style}}
                    onPress={() => {
                        Alert.alert(
                            "suppressin",
                            this.props.prompt,
                            [
                                {text: "Oui",
                                    onPress: () => {
                                        this.props.callback(() => {

                                        });
                                    }},
                                {text: "Non",
                                    onPress: () => {

                                    },
                                    style: "cancel"}
                            ],
                            { cancelable: false }
                        );
                    }}
                />);
            }
            return <Text>nada</Text>;

        }
        return <Text>nada</Text>;

    }
}

AdminButton.propTypes = {

};

export default AdminButton;

