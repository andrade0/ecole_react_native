import React, { Component, PropTypes } from "react";
import { observer } from 'mobx-react';
import RouterModel from '../models/RouterModel';
import AppModel from '../models/AppModel';
import UserModel from '../models/UserModel';
import { AppRegistry, Image, StyleSheet, TextInput, Alert, Linking } from "react-native";
import { Container, Header, Content, Button, Left, Body, Title, Right, Text, View, Icon, Spinner, List, Fab, FooterTab, Badge, Footer, Accordion } from "native-base";

@observer
class Lesson extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comment: props.lesson.comment?props.lesson.comment:'',
            message: '',
            loading: false
        };
    }

    componentDidMount(){

    }

    componentWillMount(){

    }

    componentWillUnmount() {

    }

    render() {
        let comment = <View><Text style={{color: '#DDDDDD', fontFamily: 'OpenSans_Regular'}}>Commentaire du moniteur:</Text><br />{this.state.comment}</View>;
        let comment_button = null;
        if(UserModel.role === 2 || UserModel.role === 3){
            comment = <View style={{marginTop: 10}}>Commentaire: <br /><TextInputarea rows={3} style={{width: '100%', fontFamily: 'OpenSans_Regular'}} value={this.state.comment} onChange={(e) => this.setState({comment: e.target.value})} /></View>;
            comment_button = <Button style={{marginTop: 5, fontFamily: 'OpenSans_Regular'}} onPress={()=>{
                this.setState({loading: true});
                AppModel.request('comment', {lid: this.props.lesson.lid, comment: this.state.comment}, (response)=>{
                    this.setState({loading: false});
                    if(response.status === "success"){
                        RouterModel.goto('user', {user: this.props.lesson.eleve});
                    } else if(response.message){
                        this.setState({message: response.message});
                    }
                })}} label="Enregistrer le commentaire" />;
        }

        if(this.state.loading){
            return (<Spinner />);
        } else {
            return (
                <View>
                    {this.state.messages?<View>{this.state.messages}</View>:''}
                    <View style={{fontFamily: 'OpenSans_Regular'}}>{comment}</View>
                    <View style={{fontFamily: 'OpenSans_Regular'}}>{comment_button}</View>
                </View>
            );
        }
    }
}

Lesson.propTypes = {

};

export default Lesson;


