import React, { Component, PropTypes } from "react";
import { observer } from "mobx-react";
import { AppRegistry, Image, StyleSheet, TextInput } from "react-native";
import { Container, Header, Content, Form, Item, Label, Button, Input, Left, Body, Title, Right, Text, View, Card, CardItem, Icon } from "native-base";


@observer
class UserTeaser extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    componentWillMount() {

    }

    componentWillUnmount() {

    }

    render() {
        return (
            <View padder>
                <Card>
                    <CardItem style={{ display: "flex", flexDirection: "column", alignItems: "flex-start"}}>
                        <View style={{ display: "flex", flexDirection: "row", justifyContent: "center", alignItems: "center", marginBottom: 4 }}>
                            <Icon style={{color: "#000", fontSize: 16, marginRight: 4}} type="FontAwesome" name="user" />
                            <Text style={{color: "#000000", fontSize: 15, fontFamily: 'OpenSans_Regular'}}>{`${this.props.user.firstname} ${this.props.user.lastname}`}</Text>
                        </View>
                        <View style={{ display: "flex", flexDirection: "row", justifyContent: "center", alignItems: "center", marginBottom: 4 }}>
                            <Icon style={{color: "#000", fontSize: 16, marginRight: 4}} type="Ionicons" name="mail" />
                            <Text style={{color: "#8c8c8c", fontSize: 15, fontFamily: 'OpenSans_Regular'}}>{this.props.user.mail}</Text>
                        </View>
                        <View style={{ display: "flex", flexDirection: "row", justifyContent: "center", alignItems: "center", marginBottom: 4 }}>
                            <Icon style={{color: "#000", fontSize: 16, marginRight: 4}} type="Ionicons" name="pin" />
                            <Text style={{color: "#8c8c8c", fontSize: 15, fontFamily: 'OpenSans_Regular'}}>{this.props.user.adresse}</Text>
                        </View>
                        <View style={{ display: "flex", flexDirection: "row", justifyContent: "center", alignItems: "center", marginBottom: 4 }}>
                            <Icon style={{color: "#000", fontSize: 16, marginRight: 4}} type="Ionicons" name="call" />
                            <Text style={{color: "#8c8c8c", fontSize: 15, fontFamily: 'OpenSans_Regular'}}>{this.props.user.tel}</Text>
                        </View>
                    </CardItem>
                </Card>
            </View>
        );
    }
}

UserTeaser.propTypes = {

};

export default UserTeaser;
