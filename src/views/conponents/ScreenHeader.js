import React, { Component } from "react";
import { observer } from 'mobx-react';
import { Button, Text, View, Icon, Header, Left, Right, Body, Title, Subtitle } from "native-base";
import UserModel from "../../models/UserModel";
import {Image, ImageBackground} from "react-native";

@observer
class ScreenHeader extends Component {
    constructor(props) {
        super(props);
    }
    render() {

        if(this.props.nologo){
            return (<View>
                    <Header noShadow androidStatusBarColor="#C2342A" transparent style={{ backgroundColor: "transparent", borderBottomWidth: 0, marginBottom: 30 }}>
                        <Left>
                            <Button onPress={this.props.backButtonAction} transparent>
                                <Icon style={{color: "white", fontSize: 30}} type="FontAwesome" name="arrow-circle-left" />
                            </Button>
                        </Left>
                        <Body />
                        <Right />
                    </Header>
                </View>
            );
        } else {
            return (<View>
                    <Header noShadow androidStatusBarColor="#C2342A" transparent style={{ backgroundColor: "transparent", borderBottomWidth: 0, marginBottom: 30 }}>
                        <Left>
                            <Button onPress={this.props.backButtonAction} transparent>
                                <Icon style={{color: "white", fontSize: 30}} type="FontAwesome" name="arrow-circle-left" />
                            </Button>
                        </Left>
                        <Body />

                        <Right>
                            <Image style={{width: 120, height: 50}} source={require("../../images/logo2.png")} alt="logo"/>
                        </Right>
                    </Header>
                </View>
            );
        }


    }
}

ScreenHeader.propTypes = {

};

export default ScreenHeader;


