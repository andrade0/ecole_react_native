import React, { Component, PropTypes } from "react";
import { observer } from 'mobx-react';
import UserModel from '../../models/UserModel';
import { AppRegistry, View, Image, StyleSheet, Text, Button, TextInput } from 'react-native';


@observer
class Monitor extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount(){

  }

  componentWillMount(){

  }

  componentWillUnmount() {

  }

  render() {
    return (
      <View style={{display: 'flex' ,justifyContent: 'center', alignItems: 'flex-start', width: '80%', boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)', backgroundColor: '#ffffff', borderRadius: 10, padding: 10, ...this.props.style}}>
        <View style={{width: '70%'}}>
          <View style={{textTransform: 'uppercase', marginBottom: 10}}>{this.props.user.firstname+' '+this.props.user.lastname}</View>
          <View style={{fontSize: 12, marginBottom: 6}}><Image source={{ uri: "https://image.flaticon.com/icons/svg/967/967848.svg"}} style={{width: 10}} />  {this.props.user.adresse}</View>
          <View style={{fontSize: 12, marginBottom: 6}}><Image source={{ uri: "https://image.flaticon.com/icons/svg/15/15892.svg"}} style={{width: 10}} />  {this.props.user.tel}</View>
          <View style={{fontSize: 12, marginBottom: 6}}><Image source={{ uri: "https://image.flaticon.com/icons/svg/27/27630.svg"}} style={{width: 10}} />  {this.props.user.mail}</View>
          <View style={{fontSize: 12, marginBottom: 6, cursor: 'pointer', color: 'red'}} onPress={()=>{
            UserModel.logout(()=>{});
          }}>Deconexion</View>
        </View>
        <View style={{width: '30%', textAlign: 'right'}}>
          {this.props.user.gender === 'm'?<Image source={{ uri: "https://image.flaticon.com/icons/svg/145/145862.svg"}} style={{width: 50}} />:<Image source={{ uri: "https://image.flaticon.com/icons/svg/145/145867.svg"}} style={{width: 50}} />}
        </View>
      </View>
    );
  }
}

Monitor.propTypes = {

};

export default Monitor;


