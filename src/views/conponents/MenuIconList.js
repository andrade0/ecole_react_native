import React, {Component, PropTypes} from "react";
import {observer} from "mobx-react";
import {Body, Button, Icon, Left, List, ListItem, Right, Text} from "native-base";
import UserModel from "../../models/UserModel";


@observer
class MenuIconList extends Component {
    constructor() {
        super();
    }

    componentDidMount() {

    }

    componentWillMount() {

    }

    componentWillUnmount() {

    }

    render() {
        return (

            <List>
                {this.props.menu.map((menu, i) => {
                    if (!menu.role || menu.role && menu.role.indexOf(UserModel.role) > -1) {
                        return <ListItem style={{width: '90%'}} key={`${i}menu`} icon button onPress={menu.action}>
                            <Left>
                                <Button transparent>
                                    <Icon style={{color: "white", fontSize: 16}}
                                          type={menu.type ? menu.type : "FontAwesome"} name={menu.icon}/>
                                </Button>
                            </Left>
                            <Body>
                            <Text style={{fontSize: 15, color: 'white', fontFamily: 'OpenSans_Regular'}}>{menu.title}</Text>
                            </Body>
                            <Right>
                                <Icon style={{color: "#BB2625"}} name="arrow-forward"/>
                            </Right>
                        </ListItem>;
                    } else {
                        return null;
                    }
                })}
            </List>

        );
    }
}

MenuIconList.propTypes = {};

export default MenuIconList;

