import React, { Component, PropTypes } from "react";
import { observer } from 'mobx-react';
import moment from 'moment';
import RouterModel from '../models/RouterModel';
import AppModel from '../models/AppModel';
import { AppRegistry, Image, StyleSheet, TextInput, ImageBackground } from "react-native";
import {
    Container,
    Header,
    Content,
    Form,
    Item,
    Label,
    Button,
    Input,
    Left,
    Body,
    Title,
    Right,
    Text,
    View,
    List,
    ListItem,
    Icon,
    Thumbnail,
    Picker
} from "native-base";
import ScreenHeader from "./conponents/ScreenHeader";



@observer
class Top extends Component {
    constructor() {
        super();
        this.state = {
            items: [],
            month: moment().format('MM'),
            options: [
                {value: 'all', name: 'Tous'},
                {value: '01', name: 'Janvier'},
                {value: '02', name: 'Fevrier'},
                {value: '03', name: 'Mars'},
                {value: '04', name: 'Avril'},
                {value: '05', name: 'Mai'},
                {value: '06', name: 'Juin'},
                {value: '07', name: 'Juillet'},
                {value: '08', name: 'Aout'},
                {value: '09', name: 'Septembre'},
                {value: '10', name: 'Octobre'},
                {value: '11', name: 'Novembre'},
                {value: '12', name: 'Decembre'},
            ]
        };
    }

    componentWillMount(){

        AppModel.request('topMonitors', {month: this.state.month}, (response) => {
            if(response && response.data && response.status && response.status === 'success'){
                this.setState({items: response.data});
            }
        });


    }

    render() {

        const currentMonthStr = this.state.options.reduce((c, v)=>{
                    if(v.value === this.state.month){
                        c = v.name;
                    }
                    return c;
                }, '');

        let content = <Text style={{fontFamily: "OpenSans_Regular", color: "#ffffff", marginLeft: 15}}>Aucune leçon évaluer pour cette periode.</Text>
        if(this.state.items.length > 0){
            content = <List>
                        {this.state.items.map((item, i)=>{
                            return <ListItem key={item.uid}>
                                <Text style={{fontFamily: "OpenSans_Regular", color: "#ffffff"}}>{i+1}- </Text>
                                <Text style={{fontWeight: 'bold',fontFamily: "OpenSans_Regular", color: "#ffffff"}}>{item.firstname} {item.lastname} : </Text>
                                <Text style={{fontFamily: "OpenSans_Regular", color: "#ffffff"}}>{Math.round( item.note * 100 ) / 100}</Text>
                                <Text style={{fontFamily: "OpenSans_Regular", color: "#ffffff"}}> ( {item.lessons} leçons )</Text>
                            </ListItem>
                        })}
                    </List>;
        }

        return (
            <Container style={{width: "100%", height: "100%", backgroundColor: '#1D1C1C'}}>
                <ScreenHeader screenTitle="Lexique" backButtonAction={()=>{RouterModel.goto("home", {})}} />
                <Text style={{color: "#BB2625", fontSize: 30, marginLeft: 18, marginBottom: 18, fontFamily: 'OpenSans_Regular'}}>Classement moniteurs</Text>
                <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                    style={{ width: 200, backgroundColor: '#BB2625', marginLeft: 23, marginTop: 30, marginBottom: 20 }}
                    placeholder={currentMonthStr}
                    textStyle={{color: 'white', fontFamily: 'OpenSans_Regular'}}
                    placeholderStyle={{ color: "#1D1C1C", fontFamily: 'OpenSans_Regular' }}
                    selectedValue={this.state.eleve}
                    onValueChange={(val) => {
                        this.setState({month: val});
                        AppModel.request('topMonitors', {month: val}, (response) => {
                            if(response && response.data && response.status && response.status === 'success'){
                                this.setState({items: response.data});
                            }
                        });
                    }}
                >{this.state.options.map((i) => <Picker.Item key={`${i.value}eleve`} label={i.name} value={i.value} />)}
                </Picker>
                <Content padder>
                    {content}
                </Content>
            </Container>
        );
    }
}

Top.propTypes = {

};

export default Top;


