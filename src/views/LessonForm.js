import React, { Component, PropTypes } from "react";
import { observer } from "mobx-react";
import RouterModel from "../models/RouterModel";
import AppModel from "../models/AppModel";
import moment from "moment";
import async from "async";
import DatePicker from "react-native-datepicker";
import { ImageBackground, Platform, DatePickerIOS, Alert, DatePickerAndroid, TimePickerAndroid } from "react-native";
import { Container, Header, Content, Form, Item, Label, Button, Input, Left, Body, Title, Right, Text, View, List, ListItem, Icon, Thumbnail, Picker } from "native-base";
import ScreenHeader from "./conponents/ScreenHeader";
import MaterialIcons from "../fonts/MaterialIcons.ttf";
import Ionicons from "../fonts/Ionicons.ttf";


@observer
class LessonForm extends Component {
    constructor(props) {
        super(props);
        let lid = null;
        if (props.lid) {
            lid = props.lid;
        }
        this.state = {
            lid,
            duration: 1,
            start: moment().toDate(),
            eleve: null,
            monitor: null,
            eleves: [],
            monitors: [],
            message: null,
            displayMessage: false,
            duration_options: [
                {value: 0.5, name: "0h30"},
                {value: 1, name: "1h"},
                {value: 1.5, name: "1h30"},
                {value: 2, name: "2h"},
                {value: 2.5, name: "2h30"},
                {value: 3, name: "3h"}
            ],
            heures_options: [
                {value: "07", name: "07"},
                {value: "08", name: "08"},
                {value: "09", name: "09"},
                {value: "10", name: "10"},
                {value: "11", name: "11"},
                {value: "12", name: "12"},
                {value: "13", name: "13"},
                {value: "14", name: "14"},
                {value: "15", name: "15"},
                {value: "16", name: "16"},
                {value: "17", name: "17"},
                {value: "18", name: "18"},
                {value: "19", name: "19"},
                {value: "20", name: "20"}
            ],
            minutes_options: [
                {value: "00", name: "00"},
                {value: "15", name: "15"},
                {value: "30", name: "30"},
                {value: "45", name: "45"}
            ]
        };
        this.submit = this.submit.bind(this);
    }

    submit() {
        let action = "createLesson";

        const payload = {
            timing: moment(this.state.start).format("YYYY-MM-DD HH:mm"),
            duration: this.state.duration,
            eleve: this.state.eleve,
            monitor: this.state.monitor
        };

        if (this.state.lid) {
            action = "updateLesson";
            payload.lid = this.state.lid;
        }

        AppModel.request(action, payload, (response) => {
            if (response.status && response.status === "success") {
                if (this.props.user && this.props.user.uid) {
                    RouterModel.goto("user", {uid: this.props.user.uid});
                } else if (payload.eleve && payload.eleve){
                    RouterModel.goto("user", {uid: payload.eleve});
                }
            }
        });
    }


    componentWillMount() {
        async.parallel([
            (cb) => {
                AppModel.request("getEleves", null, (response) => {
                    if (response && response.data) {
                        const tab = response.data.map((u) => ({value: u.uid, name: `${u.firstname} ${u.lastname}`})).sort((x, y)=>{
                            if(x.name > y.name){
                                return 1;
                            }
                            if(x.name < y.name){
                                return -1;
                            }
                            return 0;
                        });
                        this.setState({eleves: tab, eleve: tab[0].value});
                        cb();
                    }
                });
            },
            (cb) => {
                AppModel.request("getMonitors", null, (response) => {
                    if (response && response.data) {
                        const tab = response.data.map((u) => ({value: u.uid, name: `${u.firstname} ${u.lastname}`})).sort((x, y)=>{
                            if(x.name > y.name){
                                return 1;
                            }
                            if(x.name < y.name){
                                return -1;
                            }
                            return 0;
                        });
                        this.setState({monitors: tab, monitor: tab[0].value});
                        cb();
                    }
                });
            }
        ], () => {
            if (this.props.lid) {
                AppModel.request("getLesson", {lid: this.props.lid}, (response) => {
                    if (response && response.status && response.status === "success" && response.data) {
                        this.setState({
                            start: moment(response.data.timing).toDate(),
                            duration: this.state.duration_options.reduce((c, v) => {
                                if (v.value === parseFloat(response.data.duration)) {
                                    c = v.value;
                                }
                                return c;
                            }, null),
                            eleve: this.state.eleves.reduce((c, v) => {
                                if (v.value === response.data.eleve) {
                                    c = v.value;
                                }
                                return c;
                            }, null),
                            monitor: this.state.monitors.reduce((c, v) => {
                                if (v.value === response.data.monitor) {
                                    c = v.value;
                                }
                                return c;
                            }, null)
                        });
                    }
                });
            } else if (this.props.user && this.props.user.uid) {
                if (this.props.user.role === 1) {
                    this.setState({
                        eleve: this.state.eleves.reduce((c, v) => {
                            if (v.value === this.props.user.uid) {
                                c = v.value;
                            }
                            return c;
                        }, null)
                    });
                } else if (this.props.user.role === 2) {
                    this.setState({
                        monitor: this.state.monitors.reduce((c, v) => {
                            if (v.value === this.props.user.uid) {
                                c = v.value;
                            }
                            return c;
                        }, null)
                    });
                }
            }
        });
    }


    componentWillUnmount() {

    }




    render() {
        const isIOS = Platform.OS === 'ios';
        let dateField = <View style={{display: 'flex', flexDirection: 'column'}}>
            <View style={{ backgroundColor: '#1D1C1C', display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Button onPress={()=>{
                    try {
                        DatePickerAndroid.open({
                            date: this.state.start
                        }).then(({action, year, month, day})=>{
                            if (action !== DatePickerAndroid.dismissedAction) {
                                const d = new Date(year, month, day);
                                this.setState({start: d});
                            }
                        });
                    } catch ({code, message}) {
                        console.log('Cannot open date picker', message);
                    }
                }} transparent>
                    <Icon style={{color: "#ffffff", fontSize: 25}} type="FontAwesome" name="calendar" />
                </Button>
                <Text style={{color: '#ffffff'}}>{("0" + this.state.start.getDate()).slice(-2)}/{("0" + (this.state.start.getMonth() + 1)).slice(-2)}/{this.state.start.getFullYear()}</Text>
            </View>
            <View style={{ backgroundColor: '#1D1C1C', display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Button onPress={()=>{
                    try {
                        TimePickerAndroid.open({
                            hour: this.state.start.getHours(),
                            minute: this.state.start.getMinutes(),
                            is24Hour: true, // Will display '2 PM'
                        }).then(({action, hour, minute})=>{
                            if (action !== DatePickerAndroid.dismissedAction) {
                                const d = new Date(this.state.start.getFullYear(), this.state.start.getMonth(), this.state.start.getDate(), hour, minute);
                                this.setState({start: d});
                            }
                        });
                    } catch ({code, message}) {
                        console.log('Cannot open date picker', message);
                    }
                }} transparent>
                    <Icon style={{color: "#ffffff", fontSize: 25}} type="Ionicons" name="clock" />
                </Button>
                <Text style={{color: '#ffffff'}}>{("0" + (this.state.start.getHours() + 1)).slice(-2)}:{this.state.start.getMinutes()}</Text>
            </View>
        </View>;

        if(isIOS){
            dateField = <View style={{backgroundColor: 'white', display: "flex", justifyContent: "space-between", alignItems: "center", flexDirection: "row", marginBottom: 10}}><DatePickerIOS
                style={{width: '100%'}}
                date={this.state.start}
                mode="datetime"
                locale="fr"
                placeholder="select date"
                format="YYYY-MM-DD HH:mm"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                    dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 4,
                        marginLeft: 0
                    },
                    dateInput: {
                        marginLeft: 36
                    }
                    // ... You can check the source to find the other keys.
                }}
                onDateChange={(date) => {
                    this.setState({start: date});
                }}
            /></View>;
        }

        let title = "Ajouter une heure de conduite";

        if (this.props.user) {
            title = `Ajouter une heure de conduite pour ${this.props.user.firstname} ${this.props.user.lastname}`;
        } else if (this.props.lid) {
            title = "Modifier une heure de conduite";
        }

        return (
            <Container style={{width: "100%", height: "100%", backgroundColor: '#1D1C1C'}}>
                <ScreenHeader textColor="#611214" screenTitle={title} backButtonAction={() => {
                    if (this.props.user) {
                        RouterModel.goto("user", {user: this.props.user});
                    } else if (this.props.lid) {
                        RouterModel.goto("user", {uid: this.state.eleve.value});
                    } else {
                        RouterModel.goto("home", null);
                    }
                }} />
                <Text style={{color: "#BB2625", fontSize: 30, marginLeft: 18, marginBottom: 18}}>Ajouter / modifier une leçon</Text>
                <Content padder>
                    <Form style={{marginTop: 30}}>
                        {dateField}
                        <View style={{display: "flex", justifyContent: "space-between", alignItems: "center", flexDirection: "row"}}>
                            <Text style={{color: 'white', fontFamily: 'OpenSans_Regular'}}>Durée</Text>
                            <View><Picker
                                mode="dropdown"
                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                style={{ width: 200 }}
                                textStyle={{color: 'white', fontFamily: 'OpenSans_Regular'}}
                                placeholderIconColor="white"
                                selectedValue={this.state.duration}
                                onValueChange={(val) => {
                                    this.setState({duration: val});
                                }}
                            >{this.state.duration_options.map((i) => <Picker.Item key={`${i.value}duration_options`} label={i.name} value={i.value} />)}
                            </Picker>
                            </View>
                        </View>
                        <View style={{display: "flex", justifyContent: "space-between", alignItems: "center", flexDirection: "row"}}>
                            <Text style={{color: 'white', fontFamily: 'OpenSans_Regular'}}>Elève</Text>
                            <View><Picker
                                mode="dropdown"
                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                style={{ width: 200 }}
                                placeholder="Elève"
                                textStyle={{color: 'white', fontFamily: 'OpenSans_Regular'}}
                                placeholderStyle={{ color: "#bfc6ea", fontFamily: 'OpenSans_Regular' }}
                                placeholderIconColor="white"
                                selectedValue={this.state.eleve}
                                onValueChange={(val) => this.setState({eleve: val})}
                            >{this.state.eleves.map((i) => <Picker.Item key={`${i.value}eleve`} label={i.name} value={i.value} />)}
                            </Picker>
                            </View>
                        </View>
                        <View style={{display: "flex", justifyContent: "space-between", alignItems: "center", flexDirection: "row"}}>
                            <Text style={{color: 'white', fontFamily: 'OpenSans_Regular'}}>Moniteur</Text>
                            <View><Picker
                                mode="dropdown"
                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                style={{ width: 200 }}
                                textStyle={{color: 'white', fontFamily: 'OpenSans_Regular'}}
                                placeholder="Moniteur"
                                placeholderStyle={{ color: "#bfc6ea", fontFamily: 'OpenSans_Regular' }}
                                placeholderIconColor="white"
                                selectedValue={this.state.monitor}
                                onValueChange={(val) => this.setState({monitor: val})}
                            >{this.state.monitors.map((i) => <Picker.Item key={`${i.value}monitor`} label={i.name} value={i.value} />)}
                            </Picker>
                            </View>
                        </View>
                        {this.state.displayMessage && this.state.message && this.state.message.length ? <Text style={{marginTop: 6, marginBottom: 6, border: "solid 1px #DDD", padding: 6, color: "#ffffff", fontFamily: 'OpenSans_Regular'}}>{this.state.message}</Text> : null}
                        <Button onPress={this.submit} style={{backgroundColor:"#C2342A", marginTop: 50}} block><Text style={{fontFamily: 'OpenSans_Regular'}}>{this.state.lid ? "Mettre à jour" : "Ajouter"}</Text></Button>
                    </Form>
                </Content>
            </Container>
        );
    }
}

LessonForm.propTypes = {

};

export default LessonForm;

