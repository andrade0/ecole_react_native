import React, { Component, PropTypes } from "react";
import { observer } from "mobx-react";
import {Linking, ImageBackground, Dimensions, Image} from "react-native";
import { Container, Header, Content, Title, Text, View } from "native-base";
import UserModel from "../models/UserModel";
import MenuIconList from './conponents/MenuIconList';
import RouterModel from '../models/RouterModel';
import UserTeaser from "./conponents/UserTeaser";

@observer
class Home extends Component {
    constructor() {
        super();
        this.state = { menu: [
            {
                title: "Mon compte",
                icon: "calendar",
                action: ()=>{RouterModel.goto('user', {user: UserModel})},
                role: [1],
                type: "Ionicons"
            },
            {
                title: "Liste des élèves",
                icon: "users",
                action: ()=>{RouterModel.goto('users', {})},
                role: [2]
            },
            {
                title: "Utilisateurs",
                icon: "users",
                action: ()=>{RouterModel.goto("users", {})},
                role: [3]
            },
            {
                title: "Ajouter une heure de conduite",
                icon: "plus",
                action: ()=>{RouterModel.goto("lessonForm", {})},
                role: [3]
            },
            {
                title: "Ajouter un utilisateur",
                icon: "user-plus",
                action: ()=>{RouterModel.goto("userForm", {})},
                role: [3]
            },
            {
                title: "Lexique",
                icon: "book",
                action: ()=>{RouterModel.goto("lexique", {})},
                role: [1, 2, 3]
            },
            {
                title: "Videos",
                icon: "videocam",
                action: ()=>{RouterModel.goto("videos", {})},
                role: [1, 2, 3],
                type: "Ionicons"
            },
            {
                title: "Informations pratiques",
                icon: "help",
                type: "Ionicons",
                action: ()=>{RouterModel.goto("info", {})},
                role: [1, 2, 3]
            },
            {
                title: "A propos de l'app",
                icon: "text",
                action: ()=>{RouterModel.goto("about", {})},
                role: [1, 2, 3],
                type: "Ionicons"
            },
            {
                title: "Contact",
                icon: "call",
                action: ()=>{Linking.openURL("tel:+33231810847")},
                role: [1, 2, 3],
                type: "Ionicons"
            },
            {
                title: "Classement moniteurs",
                icon: "pulse",
                action: ()=>{RouterModel.goto("top", {})},
                role: [2, 3],
                type: "Ionicons"
            },
            {
                title: "Déconnexion",
                icon: "log-out",
                action: ()=>{UserModel.logout()},
                role: [1, 2, 3],
                type: "Ionicons"
            }
        ]}
    }

    componentDidMount() {

    }

    componentWillMount() {

    }

    componentWillUnmount() {

    }

    render() {
        let {height, width} = Dimensions.get('window');
        let role = "Elève";
        if(UserModel.role === 2){
            role = "Monitor";
        } else if(UserModel.role === 3){
            role = "Manager";
        }

        let padding = 5;
        let fontSize = 26;
        if(height>880){
          padding = 40;
          fontSize = 32;
        }

        return (<Container style={{width: '100%', height: '100%', backgroundColor: '#1D1C1C'}}>
            <Header transparent noShadow style={{backgroundColor: 'transparent', height: 140, display: 'flex', flexDirection: 'row', justifyContent: 'flex-end', paddingTop: 39, paddingRight: 10, borderBottomWidth: 0}}>
                <Image style={{width: 120, height: 50}} source={require("../images/logo2.png")} alt="logo"/>
            </Header>
            <Text style={{color: "#BB2625", fontSize: 40, marginLeft: 18, marginBottom: 18}}>MENU</Text>
            <Content style={{paddingTop: 0}}>
                {/*<UserTeaser user={UserModel} />*/}
                <MenuIconList menu={this.state.menu} />
            </Content>
            </Container>);
    }
}

Home.propTypes = {

};

/*{
                title: "Videos",
                icon: "videocam",
                action: ()=>{RouterModel.goto("videos", {})},
                role: [1, 2, 3],
                type: "Ionicons"
            },*/

export default Home;

