import React, { Component, PropTypes } from "react";
import { observer } from "mobx-react";
import RouterModel from "../models/RouterModel";
import AppModel from "../models/AppModel";
import UserModel from "../models/UserModel";
import UserListed from "./conponents/UserListed";
import ScreenHeader from "./conponents/ScreenHeader";
import { ImageBackground, Modal, TouchableHighlight, Alert } from "react-native";
import { Container, Header, Footer, FooterTab, Content, Item, Button, Input, Text, List, Icon, View, Form, Left, Body, Right, Title } from "native-base";

@observer
class Users extends Component {
    constructor() {
        super();
        this.state = {
            users: [],
            filtered_users: [],
            filter: "",
            modalVisible: false
        };
        this.releoadUsers = this.releoadUsers.bind(this);
        this.setModalVisible = this.setModalVisible.bind(this);
        this.changeTxt = this.changeTxt.bind(this);
    }

    changeTxt(q){
        let newFilteredUsers = [];
        if (q.length > 1) {
            newFilteredUsers = this.state.users.reduce((c, v, i, a) => {
                let match = false;
                if ((v.firstname && v.firstname.length && v.firstname.match(new RegExp(q, "i"))) || (v.lastname && v.lastname.length && v.lastname.match(new RegExp(q, "i")))) {
                    match = true;
                }
                if (match) {
                    c.push(v);
                }
                return c;
            }, []);
        } else {
            newFilteredUsers = this.state.users;
        }
        this.setState({
            filtered_users: newFilteredUsers,
            filter: q,
        });
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    releoadUsers() {
        let action = "getUsers";
        if (UserModel.role === 2) {
            action = "getEleves";
        }
        AppModel.request(action, null, (response) => {
            if (response && response.data) {
                this.setState({
                    users: response.data,
                    filtered_users: response.data
                });
            }
        });
    }

    componentWillMount() {
        let action = "getUsers";
        if (UserModel.role === 2) {
            action = "getEleves";
        }
        AppModel.request(action, null, (response) => {
            if (response && response.data) {
                this.setState({
                    users: response.data.sort((x, y) => {
                            if(x.firstname > y.firstname) {
                                return 1;
                            }
                            if(x.firstname < y.firstname) {
                                return -1;
                            }
                            return 0;
                        }),
                    filtered_users: response.data
                });
            }
        });
    }

    render() {

        return (
            <Container style={{width: "100%", height: "100%", backgroundColor: '#1D1C1C'}}>
                <ScreenHeader textColor="#ffffff" bigTitle="Liste des élèves" backButtonAction={() => { RouterModel.goto("home", null); }} />
                <Text style={{color: "#BB2625", fontSize: 40, marginLeft: 18, fontFamily: 'OpenSans_Regular'}}>Liste des élèves</Text>
                <Form rounded style={{marginTop: 0, marginBottom: 20, marginTop: 20, width: 300, marginLeft: 20}}>
                    <Item rounded style={{borderColor: "#FFFFFF", width: '100%', height: 30}}>
                        <Icon style={{color: "#ffffff", fontSize: 20}} name="ios-search" />
                        <Input
                            style={{ color: "#ffffff", fontSize: 12, fontFamily: "OpenSans_Regular" }}
                            placeholderStyle={{ color: "#ffffff", fontSize: 12, fontFamily: "OpenSans_Regular" }}
                            placeholderTextColor="#ffffff"
                            onFocus={() => { /*this.setModalVisible(true);*/ }}
                            value={this.state.filter}
                            onChangeText={this.changeTxt}
                            placeholder="Rechercher"
                        />
                    </Item>
                </Form>
                <Content>
                    <List>
                        {this.state.filtered_users.map((user) => <UserListed releoadUsers={this.releoadUsers} user={user} key={user.uid} />)}
                    </List>
                </Content>

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                    }}
                >

                    <View padder style={{height: "100%", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center"}}>
                        <View style={{position: "absolute", top: 20, right: 10}}><Button
                            transparent
                            onPress={() => {
                                this.setState({modalVisible: false});
                            }}
                        ><Icon style={{color: "#000000", fontSize: 30}} type="FontAwesome" name="times" />
                        </Button>
                        </View>
                        <Text style={{marginBottom: 70, fontSize: 28, color: "#767676", fontFamily: "OpenSans_Regular"}}>Rechercher un élève</Text>

                        <Item style={{marginTop: 80, marginLeft: 20, marginRight: 20, borderColor: "#C2342A"}}>
                            <Input
                                style={{width: 200, fontSize: 20}}
                                value={this.state.filter}
                                placeholder="Rechercher ..."
                                placeholderStyle={{ color: "#767676", fontSize: 20, fontFamily: "OpenSans_Regular" }}
                                onChangeText={(text) => {
                                    this.setState({
                                        filter: text
                                    });
                                }}
                            /><Button
                            transparent
                            onPress={() => {
                                let newFilteredUsers = [];
                                if (this.state.filter.length > 1) {
                                    newFilteredUsers = this.state.users.reduce((c, v, i, a) => {
                                        let match = false;
                                        if (v.firstname && v.firstname.length && v.firstname.match(new RegExp(this.state.filter, "i")) || v.lastname && v.lastname.length && v.lastname.match(new RegExp(this.state.filter, "i"))) {
                                            match = true;
                                        }
                                        if (match) {
                                            c.push(v);
                                        }
                                        return c;
                                    }, []);
                                } else {
                                    newFilteredUsers = this.state.users;
                                }
                                this.setState({
                                    filtered_users: newFilteredUsers,
                                    modalVisible: false
                                });
                            }}
                        ><Icon style={{color: "#000000"}} name="ios-search" />
                        </Button>
                        </Item>
                    </View>
                </Modal>
            </Container>
        );
    }
}

Users.propTypes = {

};

export default Users;

