import React, { Component, PropTypes } from "react";
import { observer } from 'mobx-react';
import RouterModel from '../models/RouterModel';
import MenuIconList from './conponents/MenuIconList';
import { AppRegistry, Image, StyleSheet, TextInput, Linking, ImageBackground } from "react-native";
import { Container, Header, Content, Form, Item, Label, Button, Input, Left, Body, Title, Right, Text, View, List, ListItem, Icon, Thumbnail } from "native-base";
import ScreenHeader from "./conponents/ScreenHeader";


@observer
class About extends Component {
    constructor() {
        super();
        this.state = {menu: [
                {
                    icon: "call",
                    action: ()=>{Linking.openURL("mailto:autoecoledeauville@gmail.com")},
                    title: "Une idée pour améliorer l'app ?",
                    type: "Ionicons"
                },
                {
                    icon: "bug",
                    action: ()=>{Linking.openURL("mailto:autoecoledeauville@gmail.com")},
                    title: "Vous avez dénicher un bug ?",
                    type: "Ionicons"
                }
            ]};
    }
    render() {
         return (<Container style={{width: "100%", height: "100%", backgroundColor: '#1D1C1C'}}>
                <ScreenHeader textColor="#ffffff" screenTitle="A propos" backButtonAction={()=>{RouterModel.back()}} />
             <Text style={{color: "#BB2625", fontSize: 30, marginLeft: 18, marginBottom: 18, fontFamily: 'OpenSans_Regular'}}>A propos</Text>
                <Content style={{marginTop: 0}}>
                    <MenuIconList menu={this.state.menu} />
                </Content>
            </Container>);
    }
}

About.propTypes = {

};

export default About;


