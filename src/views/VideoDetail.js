import React, { Component, PropTypes } from "react";
import { observer } from 'mobx-react';
import RouterModel from '../models/RouterModel';
import { AppRegistry, Image, StyleSheet, TextInput } from "react-native";
import { Container, Header, Content, CardItem, Item, Thumbnail, Button, Card, Left, Body, Title, Right, Text, View, List, ListItem, Icon } from "native-base";
import ScreenHeader from "./conponents/ScreenHeader";
import { Video } from 'expo';
import VideoPlayer from '@expo/videoplayer';

@observer
class VideoDetail extends Component {
    constructor() {
        super();
    }

    render() {

        return (
            <Container style={{width: "100%", height: "100%", backgroundColor: '#1D1C1C'}}>
                <ScreenHeader textColor="#611214" screenTitle={this.props.video.title} backButtonAction={()=>{RouterModel.back()}} />
                <Content padder style={{ backgroundColor: "#fff" }}>
                    <Text style={{textAlign: 'left', marginBottom: 20, marginTop: 10, fontFamily: 'OpenSans_Regular'}}>{this.props.video.description}</Text>
                    <View style={{display: 'flex', flexDirection: 'row'}}>
                        {/*<VideoPlayer
                            showFullscreenButton={false}
                            videoProps={{
                                shouldPlay: true,
                                resizeMode: Video.RESIZE_MODE_CONTAIN,
                                source: {
                                    uri: this.props.video.uri,
                                },
                            }}
                            isPortrait={true}
                            playFromPositionMillis={0}
                        />*/}
                        <Video
                            source={{ uri: this.props.video.uri }}
                            rate={1.0}
                            volume={1.0}
                            isMuted={false}
                            resizeMode="cover"
                            shouldPlay
                            isLooping
                            useNativeControls
                            style={{ width: '100%', height: 300 }}
                        />
                    </View>
                </Content>
            </Container>
        );
    }
}

VideoDetail.propTypes = {

};

export default VideoDetail;

/*<Video source={this.props.video.uri} width="100%" controls={true} playing />*/
