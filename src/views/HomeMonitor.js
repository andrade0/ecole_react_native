import React, { Component, PropTypes } from "react";
import { observer } from 'mobx-react';
import RouterModel from '../models/RouterModel';
import AppModel from '../models/AppModel';
import UserModel from '../models/UserModel';
import Monitor from './conponents/Monitor';
import UserListed from './conponents/UserListed';
import { AppRegistry, Image, StyleSheet, TextInput } from "react-native";
import { Container, Header, Content, Form, Item, Label, Button, Input, Left, Body, Title, Right, Text, View, List, ListItem, Icon, Thumbnail, Fab } from "native-base";


@observer
class HomeMonitor extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
      filtered_users: [],
      filter: ''
    };
  }

  componentDidMount(){

  }

  componentWillMount(){
    AppModel.request('getEleves', null, (response)=>{
      if(response && response.data){
        this.setState({
          users: response.data,
          filtered_users: response.data
        });
      }
    });
  }

  componentWillUnmount() {

  }

  render() {
      return (
          <Container>
              <Header androidStatusBarColor="#C2342A" searchBar rounded style={{backgroundColor:"#C2342A"}}>
                  <Item>
                      <Icon name="ios-search" />
                      <Input
                          value={this.state.filter}
                          placeholder="Elèves ..."
                          onChangeText={(text) => {
                              let newFilteredUsers = [];
                              if (text.length > 1) {
                                  newFilteredUsers = this.state.users.reduce((c, v, i, a) => {
                                      let match = false;
                                      if (v.firstname && v.firstname.length && v.firstname.match(new RegExp(text, "i")) || v.lastname && v.lastname.length && v.lastname.match(new RegExp(text, "i"))) {
                                          match = true;
                                      }
                                      if (match) {
                                          c.push(v);
                                      }
                                      return c;
                                  }, []);
                              }
                              this.setState({
                                  filter: text,
                                  filtered_users: newFilteredUsers
                              });
                          }}
                      />
                      {this.state.filter.length>0?<Icon onPress={()=>{this.setState({filter: ""})}} type="Ionicons" name="close" />:<Icon name="ios-people" />}
                  </Item>
              </Header>
              <Content>
                  <List>
                      {this.state.filtered_users && this.state.filtered_users.length > 0?this.state.filtered_users.map((user) => <UserListed releoadUsers={this.releoadUsers} user={user} key={user.uid} />):<Text></Text>}
                  </List>
              </Content>
          </Container>
      );
  }
}

HomeMonitor.propTypes = {

};

export default HomeMonitor;


