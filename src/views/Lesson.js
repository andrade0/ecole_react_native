import React, { Component, PropTypes } from "react";
import { observer } from 'mobx-react';
import moment from "moment";
import RouterModel from '../models/RouterModel';
import AppModel from '../models/AppModel';
import UserModel from '../models/UserModel';
import {AppRegistry, Image, StyleSheet, TextInput, Alert, Linking, ImageBackground} from "react-native";
import { Container, Header, Content, Button, Left, Body, Title, Right, Text, Textarea, Form, View, Icon, Spinner, List, Fab, FooterTab, Badge, Footer, Accordion } from "native-base";
import ScreenHeader from "./conponents/ScreenHeader";
import { Rating, AirbnbRating } from 'react-native-ratings';
import {AppLoading} from "expo";


@observer
class Lesson extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comment: '',
            message: '',
            lesson: null
        };
    }

    componentWillMount() {
        if (this.props.lesson) {
            this.setState({
                lesson: this.props.lesson,
                comment: this.props.lesson.comment
            });
        } else if (this.props.lid) {
            AppModel.request("getLesson", {lid: this.props.lid}, (response) => {
                if (response && response.status && response.status === "success" && response.data) {
                    this.setState({lesson: response.data, comment: response.data.comment});
                }
            });
        }
    }

    render() {

         if (AppModel.loading ) {
            return <AppLoading />;
        }

        if (this.state.lesson) {
            const timing = AppModel.moment(this.state.lesson.timing);
            const month = timing.format("MMMM");
            const dayOfMonth = timing.format("D");
            const dayOfWeek = timing.format("dddd");
            const heure_de_la_lecon = timing.format("HH:mm");
            const title = `Leçon 4 de ${UserModel.firstname}`;
            return (
                <Container style={{width: "100%", height: "100%", backgroundColor: '#1D1C1C'}}>
                    <ScreenHeader nologo={true} subTitle={`${dayOfWeek} ${dayOfMonth} ${month} à ${heure_de_la_lecon}`} textColor="#611214" bigTitle={title} backButtonAction={()=>{RouterModel.goto("user", {uid: this.state.lesson.eleve.uid})}} />
                    <Content style={{paddingLeft: 20, paddingRight: 20}}>
                        <View style={{padding: 10}}>
                            <View style={{ display: "flex", flexDirection: "row", justifyContent: "flex-start", marginBottom: 30 }}>
                                <Text style={{color: "#BB2625", fontSize: 35, fontWeight: 'normal', fontFamily: 'OpenSans_Regular'}}>Heure de conduite</Text>
                            </View>
                            {UserModel.role === 3 || UserModel.role === 2?<View style={{display: "flex", flexDirection: "row", justifyContent: "flex-start", marginBottom: 6 }}>
                                <Image source={require('../images/homme.png')} style={{width: 26, height: 26, marginRight: 10}} />
                                <Text style={{color: "white", fontSize: 16, fontFamily: 'OpenSans_Regular'}}></Text>
                                <Text style={{color: "white", fontSize: 16, fontFamily: 'OpenSans_Regular'}}> {`${this.state.lesson.eleve.firstname} ${this.state.lesson.eleve.lastname}`}</Text>
                            </View>:null}
                            {UserModel.role === 3 || UserModel.role === 1?<View style={{ display: "flex", flexDirection: "row", justifyContent: "flex-start", marginBottom: 6 }}>
                                <Image source={require('../images/moniteur.png')} style={{width: 26, height: 26, marginRight: 10}} />
                                <Text style={{color: "white", fontSize: 16, fontFamily: 'OpenSans_Regular'}}></Text>
                                <Text style={{color: "white", fontSize: 16, fontFamily: 'OpenSans_Regular'}}> {`${this.state.lesson.monitor.firstname} ${this.state.lesson.monitor.lastname}`}</Text>
                            </View>:null}
                            <View style={{ display: "flex", flexDirection: "row", justifyContent: "flex-start", marginTop: 12, marginBottom: 12 }}>
                                <Image source={require('../images/duree.png')} style={{width: 26, height: 26, marginRight: 10}} />
                                <Text style={{color: "white", fontSize: 16, fontFamily: 'OpenSans_Regular'}}>{this.state.lesson.duration} heure(s)</Text>
                            </View>
                            <View style={{ display: "flex", flexDirection: "row", justifyContent: "flex-start", marginBottom: 6 }}>
                                <Image source={require('../images/date.png')} style={{width: 26, height: 26, marginRight: 10}} />
                                <Text style={{color: "white", fontSize: 16, fontFamily: 'OpenSans_Regular'}}>{dayOfWeek} {dayOfMonth} {month} à {heure_de_la_lecon}</Text>
                            </View>
                            {moment(this.state.lesson.timing) < moment()?<View style={{ marginTop: 20, marginBottom: 20, display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "center"}}>
                                <Text style={{color: "#BB2625", fontSize: 16, fontWeight: 'normal', fontFamily: 'OpenSans_Regular'}}>Commentaire de l'enseigant: </Text>
                            </View>:<Text></Text>}
                            {moment(this.state.lesson.timing) < moment()?<View style={{height: 180, borderRadius: 30, backgroundColor: 'white', display: "flex", flexDirection: "row", justifyContent: "flex-start", marginBottom: 6, marginTop: 6, padding: 10 }}>
                                <Text style={{color: "#000", fontSize: 13, fontFamily: 'OpenSans_Regular'}}>{this.state.lesson.comment}</Text>
                            </View>:<Text></Text>}

                        </View>
                        {UserModel.role > 1 && moment(this.state.lesson.timing) < moment()?<Form>
                            <View padder style={{marginTop: 10, marginBottom: 10, borderRadius: 30, backgroundColor: 'white'}}>
                                <Textarea style={{backgroundColor: 'transparent', borderColor: 'transparent', fontFamily: 'OpenSans_Regular'}} value={this.state.comment} onChangeText={(text) => {
                                    this.setState({comment: text});
                                }} rowSpan={5} bordered placeholder="Commentaire ..." />
                            </View>
                            <Button rounded style={{backgroundColor: "#ffffff", width: 255, marginLeft: "auto", marginRight: "auto"}} onPress={()=>{
                                AppModel.request("comment", {lid: this.state.lesson.lid, comment: this.state.comment}, (_response)=>{
                                    if(_response.status && _response.status === 'success'){
                                        AppModel.request("getLesson", {lid: this.state.lesson.lid}, (response) => {
                                            if (response && response.status && response.status === "success" && response.data) {
                                                this.setState({lesson: response.data, comment: response.data.comment});
                                                RouterModel.goto('users');
                                            }
                                        });
                                    }
                                })
                            }}>
                                <Text style={{color: "#C2342A", fontFamily: 'OpenSans_Regular'}}>Mettre à jour le commentaire</Text>
                            </Button>
                        </Form>:<Text></Text>}
                        {moment(this.state.lesson.timing) < moment()?<View style={{marginTop: 20, display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}><Text style={{fontSize: 16, color: '#ffffff'}}>Évaluez votre enseignant</Text><AirbnbRating
                            count={5}
                            isDisabled={UserModel.role === 2}
                            defaultRating={parseInt(this.state.lesson.rating)}
                            reviews={["Très decu(e)", "decu(e)", "Moyen", "Bien", "Top"]}
                            onFinishRating={(rate) => {
                                AppModel.request('rateLesson', {rate, lid: this.state.lesson.lid}, (response) => {

                                });
                            }}
                        /></View>:<Text></Text>}
                    </Content>
                    {UserModel.role !== 2 && UserModel.role !== 1?<Fab direction="down"
                                               position="topRight"
                                               active={this.state.active}
                                               containerStyle={{ }}
                                               style={{ backgroundColor: '#C2342A' }}
                                               onPress={() => this.setState({ active: !this.state.active })}>
                        <Icon type="Ionicons" name="cog" />
                        {UserModel.role === 3?<Button style={{ backgroundColor: '#C2342A' }} onPress={() => {
                            Alert.alert(
                                "",
                                "êtes vous sûr de vouloir supprimer cette leçon ?",
                                [
                                    {text: "Oui",
                                        onPress: () => {
                                            AppModel.request("deleteLesson", {lid: this.state.lesson.lid}, () => {
                                                RouterModel.goto('users', {});
                                            });
                                        }},
                                    {text: "Non",
                                        onPress: () => {

                                        },
                                        style: "cancel"}
                                ],
                                { cancelable: false }
                            );
                        }}>
                            <Icon type="Ionicons" name="trash" />
                        </Button>:null}
                        {UserModel.role === 3?<Button style={{ backgroundColor: "#C2342A" }} onPress={()=>{
                            RouterModel.goto("lessonForm", {lid: this.state.lesson.lid});
                        }}>
                            <Icon type="FontAwesome" name="edit" />
                        </Button>:null}

                    </Fab>:<Text></Text>}
                </Container>
            );
        }
        return (<Spinner />);
    }
}

Lesson.propTypes = {

};

export default Lesson;




