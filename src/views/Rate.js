import React, { Component, PropTypes } from "react";
import { observer } from 'mobx-react';
import RouterModel from '../models/RouterModel';
import AppModel from '../models/AppModel';
import UserModel from '../models/UserModel';
import {AppRegistry, Image, StyleSheet, TextInput, Alert, Linking, ImageBackground} from "react-native";
import { Container, Header, Content, Button, Left, Body, Title, Right, Text, Textarea, Form, View, Icon, Spinner, List, Fab, FooterTab, Badge, Footer, Accordion } from "native-base";
import ScreenHeader from "./conponents/ScreenHeader";
import { Rating, AirbnbRating } from 'react-native-ratings';


@observer
class Rate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lesson: ''
        };
    }

    componentWillMount() {
        if (this.props.lesson) {
            this.setState({lesson: this.props.lesson, comment: this.props.lesson.comment});
        } else if (this.props.lid) {
            AppModel.request("getLesson", {lid: this.props.lid}, (response) => {
                if (response && response.status && response.status === "success" && response.data) {
                    this.setState({lesson: response.data, comment: response.data.comment});
                }
            });
        }
    }

    render() {
        if (this.state.lesson) {
            const timing = AppModel.moment(this.state.lesson.timing);
            const month = timing.format("MMMM");
            const dayOfMonth = timing.format("D");
            const dayOfWeek = timing.format("dddd");
            const heure_de_la_lecon = timing.format("HH:mm");
            const title = `Leçon 4 de ${UserModel.firstname}`;
            return (
                <Container style={{width: "100%", height: "100%", backgroundColor: '#1D1C1C'}}>
                    <ScreenHeader nologo={true} subTitle={`${dayOfWeek} ${dayOfMonth} ${month} à ${heure_de_la_lecon}`} textColor="#611214" bigTitle={title} backButtonAction={()=>{RouterModel.goto("user", {uid: this.state.lesson.eleve.uid})}} />
                    <Content style={{paddingLeft: 20, paddingRight: 20}}>
                        <View style={{padding: 10, marginBottom: 100}}>
                            <View style={{ display: "flex", flexDirection: "row", justifyContent: "flex-start", marginBottom: 30 }}>
                                <Text style={{color: "#BB2625", fontSize: 20, fontWeight: 'normal', fontFamily: 'OpenSans_Regular'}}>Evaluez votre heure de conduite</Text>
                            </View>
                            {UserModel.role === 3 || UserModel.role === 2?<View style={{display: "flex", flexDirection: "row", justifyContent: "flex-start", marginBottom: 6 }}>
                                <Image source={require('../images/homme.png')} style={{width: 26, height: 26, marginRight: 10}} />
                                <Text style={{color: "white", fontSize: 16, fontFamily: 'OpenSans_Regular'}}></Text>
                                <Text style={{color: "white", fontSize: 16, fontFamily: 'OpenSans_Regular'}}> {`${this.state.lesson.eleve.firstname} ${this.state.lesson.eleve.lastname}`}</Text>
                            </View>:null}
                            {UserModel.role === 3 || UserModel.role === 1?<View style={{ display: "flex", flexDirection: "row", justifyContent: "flex-start", marginBottom: 6 }}>
                                <Image source={require('../images/moniteur.png')} style={{width: 26, height: 26, marginRight: 10}} />
                                <Text style={{color: "white", fontSize: 16, fontFamily: 'OpenSans_Regular'}}></Text>
                                <Text style={{color: "white", fontSize: 16, fontFamily: 'OpenSans_Regular'}}> {`${this.state.lesson.monitor.firstname} ${this.state.lesson.monitor.lastname}`}</Text>
                            </View>:null}
                            <View style={{ display: "flex", flexDirection: "row", justifyContent: "flex-start", marginTop: 12, marginBottom: 12 }}>
                                <Image source={require('../images/duree.png')} style={{width: 26, height: 26, marginRight: 10}} />
                                <Text style={{color: "white", fontSize: 16, fontFamily: 'OpenSans_Regular'}}>{this.state.lesson.duration} heure(s)</Text>
                            </View>
                            <View style={{ display: "flex", flexDirection: "row", justifyContent: "flex-start", marginBottom: 6 }}>
                                <Image source={require('../images/date.png')} style={{width: 26, height: 26, marginRight: 10}} />
                                <Text style={{color: "white", fontSize: 16, fontFamily: 'OpenSans_Regular'}}>{dayOfWeek} {dayOfMonth} {month} à {heure_de_la_lecon}</Text>
                            </View>
                        </View>
                        {UserModel.role === 1?<AirbnbRating
                            count={5}
                            reviews={["Mauvais", "Moyen", "Agreable", "Utile", "superbe"]}
                            onFinishRating={(rate) => {
                                AppModel.request('rateLesson', {rate, lid: this.state.lesson.lid}, (response) => {

                                });
                            }}
                        />:<Text></Text>}
                    </Content>
                </Container>
            );
        }
        return (<Spinner />);
    }
}

Rate.propTypes = {

};

export default Rate;




