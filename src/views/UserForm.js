import React, { Component, PropTypes } from "react";
import { observer } from "mobx-react";
import RouterModel from "../models/RouterModel";
import AppModel from "../models/AppModel";
import { ImagePicker } from 'expo';
import DatePicker from "react-native-datepicker";
import { AppRegistry, Image, StyleSheet, TextInput } from "react-native";
import {
    Container,
    Header,
    Content,
    Form,
    Item,
    Label,
    Button,
    Input,
    Left,
    Body,
    Title,
    Right,
    Text,
    View,
    List,
    ListItem,
    Icon,
    Thumbnail,
    Picker,
    Toast
} from "native-base";
import ScreenHeader from "./conponents/ScreenHeader";


@observer
class UserForm extends Component {
    constructor(props) {
        super(props);
        let uid = null;
        if (props.uid) {
            uid = props.uid;
        }
        this.state = {
            uid,
            firstname: "",
            image: null,
            lastname: "",
            gender: "m",
            evaluationDepart: "",
            adresse: "",
            mail: "",
            password: "",
            tel: "",
            role: 1,
            message: "",
            displayMessage: false,
            roles: [
                {value: 1, name: "Elève"},
                {value: 2, name: "Monitor"},
                {value: 3, name: "Manager"}
            ],
            genders: [
                {value: "m", name: "Homme"},
                {value: "f", name: "Femme"}
            ]
        };
        this.submit = this.submit.bind(this);
        this._pickImage = this._pickImage.bind(this);
    }

    submit() {
        let action = "createUser";
        const payload = {
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            gender: this.state.gender,
            evaluationDepart: this.state.evaluationDepart.replace(/[^\d.-]/g, ""),
            adresse: this.state.adresse,
            mail: this.state.mail,
            password: this.state.password,
            tel: this.state.tel,
            role: this.state.role,
            image: this.state.image
        };
        if (this.state.uid) {
            action = "updateUser";
            payload.uid = this.state.uid;
        }

        if(isNaN(parseInt(this.state.evaluationDepart))){
            Toast.show({
                text: "l'évaluation de départ doit être un nombre",
                position: "top",
                duration: 3000,
                type: "success"
            });
        } else {
            AppModel.request(action, payload, (response) => {
                if(response && response.status && response.status === "success"){
                    RouterModel.goto("users", {});
                }
            });
        }


    }

    componentWillMount() {
        if (this.props.uid) {
            AppModel.request("getUser", {uid: this.props.uid}, (response) => {
                if (response && response.status && response.status === "success" && response.data) {
                    this.setState({
                        firstname: response.data.firstname,
                        lastname: response.data.lastname,
                        gender: this.state.genders.reduce((c, v) => {
                            if (v.value === response.data.gender) {
                                c = v.value;
                            }
                            return c;
                        }, null),
                        evaluationDepart: response.data.evaluationDepart,
                        adresse: response.data.adresse,
                        image: response.data.picture,
                        mail: response.data.mail,
                        tel: response.data.tel,
                        role: this.state.roles.reduce((c, v) => {
                            if (v.value === parseInt(response.data.role)) {
                                c = v.value;
                            }
                            return c;
                        }, null)
                    });
                } else {
                    cb();
                }
            });
        }
    }

    _pickImage = async () => {
        this.setState({ image: null });
        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 3],
            base64: true
        });
        if (!result.cancelled) {
            let base64Img = `data:image/jpg;base64,${result.base64}`;
            AppModel.upload(base64Img, (response)=>{
                if(response && response.image){
                    this.setState({ image: response.image });
                }
            });
        }
    };

    render() {
        let title = "Ajouter un utilisateur";

        if (this.state.firstname && this.state.lastname) {
            title = `Modifier le profile de ${this.state.firstname} ${this.state.lastname}`;
        }
        return (
            <Container style={{width: "100%", height: "100%", backgroundColor: '#1D1C1C'}}>
                <ScreenHeader textColor="#611214" screenTitle={title} backButtonAction={() => {
                    if (this.props.uid) {
                        RouterModel.goto("users", {});
                    } else {
                        RouterModel.goto("home", null);
                    }
                }} />
                <Text style={{color: "#BB2625", fontSize: 24, marginLeft: 18, marginBottom: 18}}>Ajouter / modifier un utilisateur</Text>
                <Content padder>
                    <Form>
                        {/*<Item style={{display: 'flex', flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-start'}}>
                        <Button onPress={this._pickImage}><Text>Ajouter une photo de profile</Text></Button>
                        {this.state.image && this.state.image.length > 0 ? <View><Image source={{ uri: this.state.image }} style={{ width: 100, height: 100, marginTop: 5, marginBottom: 5 }} /></View>:null}
                        </Item>*/}
                        <Item stackedLabel style={{width: '90%'}}>
                            <Label style={{color: 'white', fontFamily: 'OpenSans_Regular'}}>Prenom</Label>
                            <Input style={{color: 'white', fontFamily: 'OpenSans_Regular'}} value={this.state.firstname ? this.state.firstname : ""} onChangeText={(text) => this.setState({firstname: text})} />
                        </Item>
                        <Item stackedLabel style={{width: '90%'}}>
                            <Label style={{color: 'white', fontFamily: 'OpenSans_Regular'}}>Nom</Label>
                            <Input style={{color: 'white', fontFamily: 'OpenSans_Regular'}} value={this.state.lastname ? this.state.lastname : ""} onChangeText={(text) => this.setState({lastname: text})} />
                        </Item>
                        <Item style={{width: '90%', display: "flex", justifyContent: "space-between", alignItems: "center", flexDirection: "row", marginLeft: 16, marginTop: 20}}>
                            <Label style={{color: 'white', fontFamily: 'OpenSans_Regular'}}>Role</Label>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                style={{ width: 200 }}
                                textStyle={{color: 'white', fontFamily: 'OpenSans_Regular'}}
                                placeholderIconColor="#BB2625"
                                selectedValue={this.state.role}
                                onValueChange={(val) => this.setState({role: val})}
                            >{this.state.roles.map((i) => <Picker.Item key={`${i.value}roles`} label={i.name} value={i.value} />)}
                            </Picker>
                        </Item>
                        <Item style={{width: '90%', display: "flex", justifyContent: "space-between", alignItems: "center", flexDirection: "row", marginLeft: 16, marginTop: 20}}>
                            <Label style={{color: 'white', fontFamily: 'OpenSans_Regular'}}>Genre</Label>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                textStyle={{color: 'white', fontFamily: 'OpenSans_Regular'}}
                                style={{ width: 200 }}
                                placeholderIconColor="#BB2625"
                                selectedValue={this.state.gender}
                                onValueChange={(val) => this.setState({gender: val})}
                            >{this.state.genders.map((i) => <Picker.Item key={`${i.value}genre`} label={i.name} value={i.value} />)}
                            </Picker>
                        </Item>
                        <Item stackedLabel style={{width: '90%'}}>
                            <Label style={{color: 'white', fontFamily: 'OpenSans_Regular'}}>Email</Label>
                            <Input style={{color: 'white', fontFamily: 'OpenSans_Regular'}} value={this.state.mail ? this.state.mail : ""} onChangeText={(text) => this.setState({mail: text})} />
                        </Item>
                        <Item stackedLabel style={{width: '90%'}}>
                            <Label style={{color: 'white', fontFamily: 'OpenSans_Regular'}}>Password</Label>
                            <Input style={{color: 'white', fontFamily: 'OpenSans_Regular'}} value={this.state.password ? this.state.password : ""} onChangeText={(text) => this.setState({password: text})} />
                        </Item>
                        <Item stackedLabel style={{width: '90%'}}>
                            <Label style={{color: 'white', fontFamily: 'OpenSans_Regular'}}>Tel</Label>
                            <Input style={{color: 'white', fontFamily: 'OpenSans_Regular'}} value={this.state.tel ? this.state.tel : ""} onChangeText={(text) => this.setState({tel: text})} />
                        </Item>
                        <Item stackedLabel style={{width: '90%'}}>
                            <Label style={{color: 'white', fontFamily: 'OpenSans_Regular'}}>Evaluation de depart</Label>
                            <Input style={{color: 'white', fontFamily: 'OpenSans_Regular'}} value={this.state.evaluationDepart ? this.state.evaluationDepart.replace(/[^\d.-]/g, '') : ""} onChangeText={(text) => {
                                this.setState({evaluationDepart: text.replace(/[^\d.-]/g, '')});
                            }} />
                        </Item>
                        <Item stackedLabel style={{width: '90%'}}>
                            <Label style={{color: 'white', fontFamily: 'OpenSans_Regular'}}>Adresse</Label>
                            <Input
                                multiline
                                style={{color: 'white', fontFamily: 'OpenSans_Regular'}}
                                numberOfLines={3}
                                value={this.state.adresse}
                                onChangeText={(text) => this.setState({adresse: text})} />
                        </Item>
                        {this.state.displayMessage && this.state.message && this.state.message.length ? <Text style={{marginTop: 6, marginBottom: 6, padding: 6, color: "#ffffff"}}>{this.state.message}</Text> : null}
                        <Button onPress={this.submit} style={{backgroundColor:"#BB2625", marginTop: 30, marginBottom: 30}} block info>
                            <Text style={{fontFamily: 'OpenSans_Regular'}}>{this.state.uid ? "Mettre à jour" : "Inserer"}</Text>
                        </Button>
                    </Form>
                </Content>
            </Container>
        );
    }
}

UserForm.propTypes = {

};

export default UserForm;

