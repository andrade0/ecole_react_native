import { observable, action, intercept, computed, toJS } from 'mobx';

import Home from '../views/Home';
import Info from '../views/Info';
import Lexique from '../views/Lexique';
import About from '../views/About';
import Lesson from '../views/Lesson';
import User from '../views/User';
import Users from '../views/Users';
import HomeMonitor from '../views/HomeMonitor';
import Videos from '../views/Videos';
import LessonForm from '../views/LessonForm';
import UserForm from '../views/UserForm';
import VideoDetail from '../views/VideoDetail';
import Rate from '../views/Rate';
import Top from '../views/Top';

class RouterModel {

  screens = [
    {
      key: "home",
      roles: [1, 2, 3],
      title: "Accueil",
      component: Home,
      arguments: null
    },
    {
      key: "lessonForm",
      roles: [2, 3],
      title: "Ajouter une leçon",
      component: LessonForm,
      arguments: {lid: null}
    },
    {
      key: "lesson",
      roles: [2, 3],
      title: "Ajouter une leçon",
      component: Lesson,
      arguments: {}
    },
    {
      key: "users",
      roles: [2, 3],
      title: "Utilisateurs",
      component: Users,
      arguments: null
    },
    {
      key: "videos",
      roles: [1, 2, 3],
      title: "Videos",
      component: Videos,
      arguments: null
    },
    {
      key: "lexique",
      roles: [1, 2, 3],
      title: "Lexique",
      component: Lexique,
      arguments: null
    },
    {
      key: "lesson",
      roles: [1, 2, 3],
      title: "Heure de conduite",
      component: Lesson,
      arguments: null
    },
    {
      key: "user",
      roles: [2, 3],
      title: "Utilisateur &$",
      component: User,
      arguments: null
    },
    {
      key: "userForm",
      roles: [2, 3],
      title: "user form",
      component: UserForm,
      arguments: null
    },
    {
      key: "homeMonitor",
      roles: [2, 3],
      title: "Elèvres",
      component: HomeMonitor,
      arguments: null
    },
    {
      key: "video",
      roles: [1, 2, 3],
      title: "Video",
      component: VideoDetail,
      arguments: null
    },
    {
      key: "about",
      roles: [1, 2, 3],
      title: "About",
      component: About,
      arguments: null
    },
    {
      key: "info",
      roles: [1, 2, 3],
      title: "Info",
      component: Info,
      arguments: null
    },
    {
      key: "rate",
      roles: [1],
      title: "Rate",
      component: Rate,
      arguments: null
    },
    {
      key: "top",
      roles: [1],
      title: "Top",
      component: Top,
      arguments: null
    }
  ];
  @observable currentScreen = null;
  @observable previewsScreen = null;

  constructor() {
    this.currentScreen = this.screens.reduce((c, v)=>{
      if(v.key === "home"){
        c = v;
      }
      return c
    }, null);
  }

  goto(routeKey, args){

    this.previewsScreen = this.screens.reduce((c, v)=>{
      if(v.key === this.currentScreen.key){
        c = v;
      }
      return c
    }, null);

    this.currentScreen = this.screens.reduce((c, v)=>{
      if(v.key === routeKey){
        if(args){
          v.arguments = args;
        }
        c = v;
      }
      return c
    }, null);
  }

  back(){
    if(this.previewsScreen && this.previewsScreen.key){
      this.goto(this.previewsScreen.key, null);
    }
  }

}

const _RouterModel = new RouterModel();
export default _RouterModel;
