import {action, computed, intercept, observable, toJS} from "mobx";
import { AppLoading, Font, Permissions, Notifications } from 'expo';
import { Alert } from "react-native";
import AppModel from './AppModel';
import RouterModel from './RouterModel';
import { AsyncStorage, Platform } from 'react-native';

class UserModel {

    @observable uid = null;
    @observable mail = '';
    @observable token = '';
    @observable firstname = '';
    @observable lastname = '';
    @observable adresse = '';
    @observable tel = '';
    @observable role = 1;
    @observable loading = false;
    @observable notification = false;

    constructor() {
        AsyncStorage.getItem('token').then((token)=>{
            if(token){
                this.token = token;
                this.loading = true;
                AppModel.request("loginWithToken", {token: token}, (response)=>{
                    this.loading = false;
                    if(response.status === 'success'){
                        this.registerPushToken(response.data.uid, ()=>{

                        });
                        this.uid = response.data.uid;
                        this.mail = response.data.mail;
                        this.token = response.data.token;
                        this.firstname = response.data.firstname;
                        this.lastname = response.data.lastname;
                        this.adresse = response.data.adresse;
                        this.tel = response.data.tel;
                        this.role = parseInt(response.data.role);
                        if(this.role === 2){
                            RouterModel.goto('home', null);
                        } else {
                            RouterModel.goto('home', null);
                        }
                    }
                });
            }
        });
    }

    login(mail, password) {
        AppModel.request("login", {mail, password}, (response)=>{
            if(response.status === 'success'){
                AsyncStorage.setItem('token', response.data.token).then((response)=>{

                });
                this.registerPushToken(response.data.uid, ()=>{

                });
                this.uid = response.data.uid;
                this.mail = response.data.mail;
                this.token = response.data.token;
                this.firstname = response.data.firstname;
                this.lastname = response.data.lastname;
                this.adresse = response.data.adresse;
                this.tel = response.data.tel;
                this.role = parseInt(response.data.role);
                if(this.role === 2){
                    RouterModel.goto('home', null);
                } else {
                    RouterModel.goto('home', null);
                }
            }
        });
    }

    logout(cb) {
        AsyncStorage.removeItem('token');
        this.uid = null;
        this.mail = null;
        this.token = null;
        this.firstname = null;
        this.lastname = null;
        this.adresse = null;
        this.tel = null;
        this.role = 1;
    }

    async registerPushToken(uid, cb){



        let status = null;

        try {
            const response = await Permissions.getAsync(Permissions.NOTIFICATIONS);
            status = response.status;
            if (status !== 'granted') {

                if(status === 'denied'){

                } else if (status === 'undetermined'){
                    const response = await Permissions.askAsync(Permissions.NOTIFICATIONS);
                    status = response.status;
                }
            }
        } catch (error) {
            console.log(error);
        }

        if(status === 'granted'){

            if (Platform.OS === 'android') {
                Notifications.createChannelAndroidAsync('auto-ecole-notifications', {
                    name: "Notifications de l'auto école",
                    sound: true,
                    priority: "max",
                    vibrate: [0, 250, 250, 250]
                });
            }
        }

        Notifications.addListener((notification) => {
            if(notification.data && notification.data.lid && notification.data.lid > 0){
                RouterModel.goto('lesson', {lid: notification.data.lid})
            }
            this.notification = notification;
        });

        Notifications.getExpoPushTokenAsync().then((token)=>{
            AppModel.request("setUserPushToken", {uid, token}, (response)=>{
                cb();
            });
        }).catch((e) => {

        });

    }
}

const _UserModel = new UserModel();
export default _UserModel;


