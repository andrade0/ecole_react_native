import React, { Component, PropTypes } from "react";
import { observer } from 'mobx-react';
import RouterModel from './models/RouterModel';
import { View } from 'react-native';

@observer
class Router extends Component {
  constructor() {
    super();
  }

  componentDidMount(){

  }

  componentWillMount(){

  }

  componentWillUnmount() {

  }

  render() {
    if(RouterModel.currentScreen){
      const args = {title: RouterModel.currentScreen.title};
      if(RouterModel.currentScreen.arguments){
        for(let index in RouterModel.currentScreen.arguments){
          args[index] = RouterModel.currentScreen.arguments[index];
        }
      }
      return <RouterModel.currentScreen.component {...args} />
    } else {
      return <View>Error no screen defenied in router</View>
    }
  }
}

Router.propTypes = {

};

export default Router;


